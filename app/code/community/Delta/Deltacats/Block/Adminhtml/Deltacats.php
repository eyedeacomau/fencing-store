<?php

class Delta_Deltacats_Block_Adminhtml_Deltacats extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_deltacats';
        $this->_blockGroup = 'deltacats';
        $this->_headerText = $this->__('Item Manager');
        $this->_addButtonLabel = $this->__('Add Item');
        parent::__construct();
    }
}