<?php

class Delta_Deltacats_Block_Adminhtml_Deltacats_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

    public function __construct() {
        parent::__construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'deltacats';
        $this->_controller = 'adminhtml_deltacats';

        $this->_updateButton('save', 'label',  $this->__('Save Attribute'));
        $this->_updateButton('delete', 'label',  $this->__('Delete Attribute'));
    }

    public function getHeaderText() {
        if (Mage::registry('deltacats_data')) {
            return  $this->__('Edit Attribute');
        } else {
            return  $this->__('Add Attribute');
        }
    }

}