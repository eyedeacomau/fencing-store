<?php

class Delta_Deltacats_Block_Adminhtml_Deltacats_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{

    protected function _prepareForm()
    {
        $form     = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('deltacats_form',
                array('legend' => $this->__('Attribute information'))
        );

        $fieldset->addField('attribute_code', 'text', array(
            'label'    => $this->__('Attribute Code'),
            'class'    => 'required-entry',
            'required' => true,
            'name'     => 'attribute_code',
            'note'     => $this->__('A unique identifier used on the database. Letters and underscores only. (<em>company_attributename</em>)')
        ));

        $fieldset->addField('frontend_label', 'text', array(
            'label'    => $this->__('Title'),
            'class'    => 'required-entry',
            'required' => true,
            'name'     => 'frontend_label',
            'note'     => $this->__('The title that appears in the category edit menu.')
        ));

        $inputTypes   = Mage::getModel('eav/adminhtml_system_config_source_inputtype')->toOptionArray();
        $inputTypes[] = array('label'       => $this->__('Media Image'), 'value'       => 'image');

        $inputTypes[] = array('label' => $this->__('File'), 'value' => 'file');

        /**
         * Unset unsupported attribute types
         *
         *  @todo: support all attribute types for categories
         */
        foreach($inputTypes as $key => $inputType) {
            if('select' == $inputType['value']
                    || 'multiselect' == $inputType['value']) {
                unset($inputTypes[$key]);
            }
        }

        $fieldset->addField('frontend_input', 'select', array(
            'name'   => 'frontend_input',
            'label'  => Mage::helper('eav')->__('Catalog Input Type for Store Owner'),
            'title'  => Mage::helper('eav')->__('Catalog Input Type for Store Owner'),
            'value'  => 'text',
            'values' => $inputTypes,
            'note'   => $this->__('The method of entering data on the category edit menu.')
        ));

        $fieldset->addField('is_required', 'select', array(
            'name'   => 'is_required',
            'label'  => Mage::helper('eav')->__('Values Required'),
            'title'  => Mage::helper('eav')->__('Values Required'),
            'values' => array(
                array('label' => $this->__('Yes'), 'value' => 1),
                array('label' => $this->__('No'), 'value' => 0)
            ),
            'note'   => $this->__('Whether or not this attribute is required on the category edit screen.')
        ));

        $fieldset->addField('sort_order', 'text', array(
            'label'    =>  $this->__('Sort Order'),
            'class'    => 'required-entry',
            'required' => true,
            'name'     => 'sort_order',
            'note'     =>  $this->__('A number, from 0 to 100, to indicate the order at which the attribute
                           is shown. 0 indicates the attribute is shown first.')
        ));

        //Disable attribute_code from being edited after it has been created
        $registry = Mage::registry('deltacats_data')->getData();
        if (isset($registry['attribute_code'])) {
            $form->getElement('attribute_code')->setReadOnly(1);
        }

        // If editing, show values
        if (Mage::getSingleton('adminhtml/session')->getDeltacatsData()) {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getDeltacatsData());
            Mage::getSingleton('adminhtml/session')->setDeltacatsData(null);
        } elseif (Mage::registry('deltacats_data')) {
            $form->setValues(Mage::registry('deltacats_data')->getData());
        }
        return parent::_prepareForm();
    }

}