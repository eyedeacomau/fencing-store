<?php

class Delta_Deltacats_Block_Adminhtml_Deltacats_Edit_Tab_Grouptitle extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $categoryEavType = Mage::getSingleton('eav/config')->getEntityType('catalog_category')->getEntityTypeId();

        $attributeSetId = Mage::getModel('eav/entity_attribute_set')
                            ->load($categoryEavType, 'entity_type_id')
                            ->getAttributeSetId();

        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('deltacats_grouptitle',
                array('legend'=>  $this->__('Group Title'))
        );

        $groupCollection = Mage::getResourceModel('eav/entity_attribute_group_collection');

        $groupValues = array();

        foreach($groupCollection as $group) {
            if($attributeSetId != $group['attribute_set_id']
                    || 'General' == $group['attribute_group_name']) {
                continue;
            }
            $groupValues[] = array(
              'value' => $group['attribute_group_name'],
              'label' => $group['attribute_group_name']
            );
        }

        $fieldset->addField('attribute_group_name', 'select', array(
            'label'     =>  $this->__('Attribute Group'),
            'values'    => $groupValues,
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'attribute_group_name',
            'note'      =>  $this->__('The tab at which the attribute shows up under.')
        ));

        if ( Mage::getSingleton('adminhtml/session')->getDeltacatsData() )
        {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getDeltacatsData());
            Mage::getSingleton('adminhtml/session')->setDeltacatsData(null);
        } elseif ( Mage::registry('deltacats_data') ) {
            $form->setValues(Mage::registry('deltacats_data')->getData());
        }
        return parent::_prepareForm();
    }
}