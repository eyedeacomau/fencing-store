<?php

class Delta_Deltacats_Block_Adminhtml_Deltacats_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('deltacats_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle( $this->__('Attribute Information') );
    }

    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label'     =>  $this->__('Attribute Information'),
            'title'     =>  $this->__('Attribute Information'),
            'content'   => $this->getLayout()->createBlock('deltacats/adminhtml_deltacats_edit_tab_form')->toHtml(),
        ));

        $this->addTab('tab_title', array(
            'label'     =>  $this->__('Group Title'),
            'title'     =>  $this->__('Group Title'),
            'content'   => $this->getLayout()->createBlock('deltacats/adminhtml_deltacats_edit_tab_grouptitle')->toHtml(),
        ));
        
        return parent::_beforeToHtml();
    }
}