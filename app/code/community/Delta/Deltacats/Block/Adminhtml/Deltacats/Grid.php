<?php

class Delta_Deltacats_Block_Adminhtml_Deltacats_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    /**
     * @todo use the resource filter to show category attributes in a way that is backwards compatible
     *
     */
    protected function _prepareCollection()
    {
        $categoryEavType  = Mage::getSingleton('eav/config')->getEntityType('catalog_category')->getEntityTypeId();
        $eavAttributes    = Mage::getResourceModel('eav/entity_attribute_collection')
                                ->setEntityTypeFilter($categoryEavType)
                                ->load();
        $attributesFilter = array();

        $eavEntityAttribute = Mage::getSingleton('core/resource')->getTableName('eav_entity_attribute');
        $eavAttributeGroup  = Mage::getSingleton('core/resource')->getTableName('eav_attribute_group');

        foreach ($eavAttributes as $eavAttribute) {
            $attributesFilter[] = $eavAttribute->getAttributeCode();
        }

        $deltaAttributes = Mage::getResourceModel('eav/entity_attribute_collection')
                ->addFieldToFilter('attribute_code', array('in' => $attributesFilter));

        $deltaAttributes->getSelect()
                        ->distinct()
                        ->join(array('eea' => $eavEntityAttribute),
                                     'main_table.attribute_id = eea.attribute_id',
                                      array())
                        ->join(array('eag' => $eavAttributeGroup),
                                     'eag.attribute_group_id = eea.attribute_group_id',
                                      array('attribute_group_name'))
                        ->where('eea.entity_type_id = ?', $categoryEavType);

        $this->setCollection($deltaAttributes);

        $this->setDefaultSort('attribute_code')
             ->setDefaultDir('asc');

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('attribute_code', array(
            'header' =>  $this->__('Attribute Code'),
            'align'  => 'left',
            'width'  => '50px',
            'index'  => 'attribute_code',
        ));

        $this->addColumn('frontend_label', array(
            'header' =>  $this->__('Title'),
            'align'  => 'left',
            'width'  => '50px',
            'index'  => 'frontend_label',
        ));

        $this->addColumn('attribute_group_name', array(
            'header' =>  $this->__('Tab'),
            'align'  => 'left',
            'width'  => '50px',
            'index'  => 'attribute_group_name',
        ));

        return parent::_prepareColumns();
    }

    /**
     *  Make the row link to an edit page
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

}