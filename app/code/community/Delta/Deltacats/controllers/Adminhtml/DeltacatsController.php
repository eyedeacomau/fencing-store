<?php

class Delta_Deltacats_Adminhtml_DeltacatsController extends Mage_Adminhtml_Controller_Action
{

    protected function _initAction()
    {
        $this->loadLayout();

        return $this;
    }

    public function indexAction()
    {
        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('deltacats/adminhtml_deltacats'));
        $this->renderLayout();
    }

    public function editAction()
    {
        $this->loadLayout();

        $params = $this->getRequest()->getParams();

        $attribute = Mage::getResourceModel('eav/entity_attribute_collection');
        $categoryEavType  = Mage::getSingleton('eav/config')->getEntityType('catalog_category')->getEntityTypeId();
        $eavEntityAttribute = Mage::getSingleton('core/resource')->getTableName('eav_entity_attribute');
        $eavAttributeGroup  = Mage::getSingleton('core/resource')->getTableName('eav_attribute_group');

        $attribute->getSelect()
                ->distinct()
                ->join(array('eea' => $eavEntityAttribute), 'eea.attribute_id = main_table.attribute_id', array('sort_order'))
                ->join(array('eag' => $eavAttributeGroup), 'eag.attribute_group_id = eea.attribute_group_id', array('attribute_group_name'))
                ->where('eea.entity_type_id = ?', $categoryEavType);

        if (isset($params['id'])) {
            $attribute->getSelect()->where('main_table.attribute_id = ?', $params['id']);
            $attribute = $attribute->getFirstItem();
        }

        $this->loadLayout();
        Mage::register('deltacats_data', $attribute);
        $this->_addContent($this->getLayout()->createBlock('deltacats/adminhtml_deltacats_edit'))
                ->_addLeft($this->getLayout()->createBlock('deltacats/adminhtml_deltacats_edit_tabs'));
        $this->renderLayout();
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function saveAction()
    {
        if ($this->getRequest()->getPost()) {
            try {
                $postData     = $this->getRequest()->getPost();
                $setup        = new Mage_Eav_Model_Entity_Setup('core_setup');
                $entityTypeId = $setup->getEntityTypeId('catalog_category');

                // Save Attribute
                $setup = new Mage_Eav_Model_Entity_Setup('core_setup');

                /**
                 * Image requires a special backend type for saving images
                 */
                switch ($postData['frontend_input']) {
                    case 'image':
                        $setup->addAttribute('catalog_category', $postData['attribute_code'], array(
                            'label'      => $postData['frontend_label'],
                            'input'      => $postData['frontend_input'],
                            'required'   => $postData['is_required'],
                            'global'     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
                            'backend'    => 'catalog/category_attribute_backend_image',
                            'group'      => $postData['attribute_group_name'],
                            'sort_order' => $postData['sort_order']
                        ));
                        break;
                    case 'file':
                        $setup->addAttribute('catalog_category', $postData['attribute_code'], array(
                            'label'      => $postData['frontend_label'],
                            'input'      => $postData['frontend_input'],
                            'backend'    => 'deltacats/file',
                            'required'   => $postData['is_required'],
                            'global'     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
                            'group'      => $postData['attribute_group_name'],
                            'sort_order' => $postData['sort_order']
                        ));
                        break;
                    case 'boolean':
                        $setup->addAttribute('catalog_category', $postData['attribute_code'], array(
                            'label'      => $postData['frontend_label'],
                            'input'      => 'select',
                            'required'   => $postData['is_required'],
							//OVEDIT
							'default'	 => 1, 
							//OVEDIT EOF
                            'global'     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
                            'group'      => $postData['attribute_group_name'],
                            'sort_order' => $postData['sort_order'],
                            'source'     => 'eav/entity_attribute_source_boolean'
                        ));
                        break;
                    default:
                        //OVEDIT
						if($postData['frontend_input']=='textarea'){
							$setup->addAttribute('catalog_category', $postData['attribute_code'], array(
								'label'      => $postData['frontend_label'],
								'type'		 => 'text',
								'input'      => $postData['frontend_input'],
								'required'   => $postData['is_required'],
								'global'     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
								'group'      => $postData['attribute_group_name'],
								'sort_order' => $postData['sort_order']
							));
						}else{
							$setup->addAttribute('catalog_category', $postData['attribute_code'], array(
								'label'      => $postData['frontend_label'],
								'input'      => $postData['frontend_input'],
								'required'   => $postData['is_required'],
								'global'     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
								'group'      => $postData['attribute_group_name'],
								'sort_order' => $postData['sort_order']
							));
						}
						//OVEDIT EOF
                }

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Attribute was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setDeltacatsData(false);

                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setDeltacatsData($this->getRequest()->getPost());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        $this->_redirect('*/*/');
    }

    public function deleteAction()
    {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $setup        = new Mage_Eav_Model_Entity_Setup('core_setup');
                $entityTypeId = $setup->getEntityTypeId('catalog_category');
                $setup->removeAttribute($entityTypeId, $this->getRequest()->getParam('id'));

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }

}