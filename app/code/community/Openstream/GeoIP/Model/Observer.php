<?php

class Openstream_GeoIP_Model_Observer
{
    public function controllerFrontInitBefore($observer)
    {
        $cms_url = '/'.Mage::getStoreConfig('general/country/page_for_redirect');
        if (strpos(Mage::helper('core/url')->getCurrentUrl(), $cms_url) === false) {
            $model = Mage::getModel('geoip/country');
            if (!$model->isCountryAllowed()) {
                $response = Mage::app()->getFrontController()->getResponse();
                $response->setRedirect($cms_url);
                $response->sendResponse();
            }
        }
    }
}