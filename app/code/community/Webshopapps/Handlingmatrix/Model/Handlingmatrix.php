<?php
/**
 * Magento Webshopapps Shipping Module
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Webshopapps
 * @package    Webshopapps_Productmatrix
 * @copyright  Copyright (c) 2009 Auction Maid (http://www.webshopapps.com)
 * @license    http://www.webshopapps.com/license/license.txt
 * @author     Karen Baker <enquiries@webshopapps.com>
*/


class Webshopapps_Handlingmatrix_Model_Handlingmatrix  extends Mage_Core_Model_Abstract {
	
	protected $_default_condition_name = 'cumulative';

    protected $_conditionNames = array();
    protected $_code = 'handlingmatrix';
    
	public function __construct()
    {
      foreach ($this->getCode('condition_name') as $k=>$v) {
            $this->_conditionNames[] = $k;
        }   
    }
    
    public function addHandlingCosts(Mage_Shipping_Model_Rate_Request $request,  &$results) {
    	
    	if (!empty($results) &&
  			is_array($results->getAllRates()) && count($results->getAllRates()))
  		{
  			$request->setConditionName(Mage::getStoreConfig('shipping/handlingmatrix/condition_name') ? Mage::getStoreConfig('shipping/handlingmatrix/condition_name') : $this->_default_condition_name);

  			$applyHandlingToFreeShipping = Mage::getStoreConfig('shipping/handlingmatrix/zero_shipping');
		  	$handlingRateArr = Mage::getResourceModel('handlingmatrix/handlingmatrix')->getHandlingRate($request);  
	        if (empty($handlingRateArr)) {
	        	return;
	        }
	        $rates = $results->getAllRates();    	
	        
	    	foreach ($rates as $key=>$rate) {
	    		foreach ($handlingRateArr as $k=>$carrierRate){	    			
	    			if($k == $rate['method_title'] || $k == ''){
	    				if($carrierRate['price'] == -1){
	    					unset($rates[$key]['carrier']);
	    					unset($rates[$key]['method']); 
	    				}
		    			else if ($rate['price']>0) {	    				
		    				$rates[$key]['price']+=$carrierRate['price'];	  
		    				if ($carrierRate['percentage']!=0) {
		    					$rates[$key]['price'] += ($rates[$key]['price']/100)* $carrierRate['percentage'];   	
		    				}  				
		    			}
		    			else if ($applyHandlingToFreeShipping){
		    				$rates[$key]['price']+=$carrierRate['price'];
		    			}
	    			}
	    		}
	    	}
	        
  		}
        return;
    }

 public function getCode($type, $code='')
    {
        $codes = array(

            'condition_name'=>array(
        		'cumulative'  	=> Mage::helper('shipping')->__('Cumulative Totalling'),
            	'highest'  		=> Mage::helper('shipping')->__('Highest Price Totalling'),
        		'product'  		=> Mage::helper('shipping')->__('Per Unique Product Totalling'),
        		'item'  		=> Mage::helper('shipping')->__('Per Item Totalling'),
        ),

            'condition_name_short'=>array(
            	'cumulative'  	=> Mage::helper('shipping')->__('Cumulative Totalling'),
            	'highest'  		=> Mage::helper('shipping')->__('Highest Price Totalling'),
        		'product'  		=> Mage::helper('shipping')->__('Per Product Totalling'),
        		'item'  		=> Mage::helper('shipping')->__('Per Item Totalling'),
        ),

        );

        if (!isset($codes[$type])) {
            throw Mage::exception('Mage_Shipping', Mage::helper('shipping')->__('Invalid Handling Matrix code type: %s', $type));
        }

        if (''===$code) {
            return $codes[$type];
        }

        if (!isset($codes[$type][$code])) {
            throw Mage::exception('Mage_Shipping', Mage::helper('shipping')->__('Invalid Handling Matrix code for type %s: %s', $type, $code));
        }

        return $codes[$type][$code];
    }
    
}