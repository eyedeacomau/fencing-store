<?php
/**
 * Magento Webshopapps Shipping Module
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * Shipping MatrixRates
 *
 * @category   Webshopapps
 * @package    Webshopapps_Handlingmatrix
 * @copyright  Copyright (c) 2008 Auction Maid (http://www.webshopapps.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Karen Baker <enquiries@webshopapps.com>
*/
class Webshopapps_Handlingmatrix_Model_Mysql4_Handlingmatrix extends Mage_Core_Model_Mysql4_Abstract
{
	
	private $_request;
	private $_zipSearchString;
	private $_table;
	private $_customerGroupCode;
	private $_starIncludeAll;
	private $_debug;
	
    protected function _construct()
    {
        $this->_init('shipping/handlingmatrix', 'pk');
    }

    public function getHandlingRate(Mage_Shipping_Model_Rate_Request $request)
    {
     	$handlingMatrixConfig=Mage::getStoreConfig('shipping/handlingmatrix');
 		$this->_debug = Mage::helper('wsalogger')->isDebug('Webshopapps_Handlingmatrix');
     	
        $read = $this->_getReadAdapter();

        // make global as used all around
        $this->_request=$request;
        
		$postcode = $request->getDestPostcode();
        $this->_table = Mage::getSingleton('core/resource')->getTableName('handlingmatrix/handlingmatrix');
		if ($handlingMatrixConfig['zip_range']) {
			#  Want to search for postcodes within a range
			$zipSearchString = $read->quoteInto(" AND dest_zip<=? ", $postcode).
								$read->quoteInto(" AND dest_zip_to>=? )", $postcode);
		} else {
			$zipSearchString = $read->quoteInto(" AND ? LIKE dest_zip )", $postcode);
		}

        $this->_zipSearchString=$zipSearchString;
        $this->_starIncludeAll=$handlingMatrixConfig['star_include_all'];
        
        $items = $request->getAllItems();
		if (empty($items) || ($items=="")) {
			return;
		}
        
        $conditionName=$this->_request->getConditionName();
        $totalFee=array();
        $structuredItems = $this->getStructuredItems($items,$conditionName);
        foreach ($structuredItems as $structuredItem) {
        	if ($structuredItem['handling_id']=='none' && $this->_starIncludeAll) { continue; }
        	$itemTotal=$this->runSelectStmt($read,$structuredItem,$conditionName);
        	
        	Mage::helper('wsalogger/log')->postInfo('handlingmatrix','Handling Charge for Structured Item:'.$structuredItem['handling_id'],$itemTotal,$this->_debug);
			
        	if(is_array($itemTotal)){
	        	foreach ($itemTotal as $key=>$itemMethod){
	        		if (!empty($itemMethod['price']) || !empty($itemMethod['percentage']) ) {
	        			if(!array_key_exists($key, $totalFee)){
	        				$totalFee[$key]=array(
	        					'price' => 0,
	        					'percentage' => 0,
	        				);
	        			}
	        			if ($conditionName=='highest') {
	        				if ($itemMethod['price']>$totalFee[$key]['price']) {
	        					$totalFee[$key]['price']=$itemMethod['price'];
	        					// no support for percentage of ship price here
	        				}
	        			} else {
	        				$totalFee[$key]['price']+=$itemMethod['price'];
	        				$totalFee[$key]['percentage']+=$itemMethod['percentage'];
	        			}        				
	        		}
	        	}
        	}
        }
    	if ($this->_debug) {
			Mage::helper('wsalogger/log')->postInfo('handlingmatrix','Cart Total Handling Charge',$totalFee);				
		}
		if (empty($totalFee)) { return; }		
		return $totalFee;
    }
    
	private function runSelectStmt($read,$structuredItem,$conditionName) 
	{
		for ($j=0;$j<9;$j++)
		{

			$select = $read->select()->from($this->_table);
			switch($j) {
				case 0:
					$select->where(
						$read->quoteInto(" (dest_country_id=? ", $this->_request->getDestCountryId()).
							$read->quoteInto(" AND dest_region_id=? ", $this->_request->getDestRegionId()).
							$read->quoteInto(" AND STRCMP(LOWER(dest_city),LOWER(?)) = 0  ", $this->_request->getDestCity()).
						#    $read->quoteInto(" AND ? LIKE dest_zip ) ", $postcode)
							$this->_zipSearchString
						);
					break;
				case 1:
					$select->where(
						$read->quoteInto(" (dest_country_id=? ", $this->_request->getDestCountryId()).
							$read->quoteInto(" AND dest_region_id=?  AND dest_city=''", $this->_request->getDestRegionId()).
							$this->_zipSearchString
						);
					break;
				case 2:
					$select->where(
						$read->quoteInto(" (dest_country_id=? ", $this->_request->getDestCountryId()).
							$read->quoteInto(" AND dest_region_id=? ", $this->_request->getDestRegionId()).
							$read->quoteInto(" AND STRCMP(LOWER(dest_city),LOWER(?)) = 0  AND dest_zip='')", $this->_request->getDestCity())
						);
					break;
				case 3:
					$select->where(
					   $read->quoteInto("  (dest_country_id=? ", $this->_request->getDestCountryId()).
							$read->quoteInto(" AND STRCMP(LOWER(dest_city),LOWER(?)) = 0  AND dest_region_id='0'", $this->_request->getDestCity()).
							$this->_zipSearchString
					   );
					break;
				case 4:
					$select->where(
					   $read->quoteInto("  (dest_country_id=? ", $this->_request->getDestCountryId()).
							$read->quoteInto(" AND STRCMP(LOWER(dest_city),LOWER(?)) = 0  AND dest_region_id='0' AND dest_zip='') ", $this->_request->getDestCity())
					   );
					break;
				case 5:
					$select->where(
						$read->quoteInto("  (dest_country_id=? AND dest_region_id='0' AND dest_city='' ", $this->_request->getDestCountryId()).
					 #  	$read->quoteInto("  AND ? LIKE dest_zip ) ", $postcode)
							$this->_zipSearchString
						);
					break;
				case 6:
					$select->where(
					   $read->quoteInto("  (dest_country_id=? ", $this->_request->getDestCountryId()).
							$read->quoteInto(" AND dest_region_id=? AND dest_city='' AND dest_zip='') ", $this->_request->getDestRegionId())
					   );
					break;

				case 7:
					$select->where(
					   $read->quoteInto("  (dest_country_id=? AND dest_region_id='0' AND dest_city='' AND dest_zip='') ", $this->_request->getDestCountryId())
					);
					break;

				case 8:
					$select->where(
							"  (dest_country_id='0' AND dest_region_id='0' AND dest_zip='')"
				);
					break;
			}
			
			if ($structuredItem['handling_id']=='none' || $structuredItem['handling_id']=='include_all' 
				|| $structuredItem['handling_id']=='WSA_ALL' ) {
				$select->where('handling_id=?','');
			} else {
				$select->where('handling_id=?', $structuredItem['handling_id']);
	
			}if ($this->_request->getConditionName()=='product'  || $this->_request->getConditionName()=='item' ){
				$select->where('item_from_value<?', $structuredItem['unique']);
				$select->where('item_to_value>=?', $structuredItem['unique']);
			}
			else{
				$select->where('item_from_value<?', $structuredItem['qty']);
				$select->where('item_to_value>=?', $structuredItem['qty']);
			}	
			
			$select->where('weight_from_value<?', $structuredItem['weight']);
			$select->where('weight_to_value>=?', $structuredItem['weight']);	
			$select->where('price_from_value<?', $structuredItem['price']);
			$select->where('price_to_value>=?', $structuredItem['price']);
								
			$select->where('website_id=?', $this->_request->getWebsiteId());
			$newdata=array();
			$row = $read->fetchAll($select);
			
			if (!empty($row))
			{
				if ($this->_debug) {
					Mage::helper('wsalogger/log')->postDebug('handlingmatrix','SQL Request',$select->getPart('where'));				
					Mage::helper('wsalogger/log')->postDebug('handlingmatrix','SQL Response',$row);				
				}
				$total;
				foreach ($row as $data) {
					$data['delivery_type'] == '*' ?	$method = '' : $method = $data['delivery_type'];
					if(!isset($total[$method])){
						$total[$method] = array(
							'price' 	 => 0,
							'percentage' => 0,
						);
					}
					$algorithms = explode('&', $data['algorithm']);	
					
					foreach ($algorithms as $algorithm){
						$a = explode('=', $algorithm);
						if (strtoupper($a[0])=="ITEM") {
							$total[$method]['price']+=$data['price']*$structuredItem['qty']; // multiple by qty
						} else if (strtoupper($a[0])=="PRODUCT" && $conditionName=='product') { 
							$total[$method]['price']+=$data['price']*$structuredItem['unique']; // multiple by unique product qty
						} else if (strtoupper($a[0])=="PERCENTAGE_CART") { 
							$total[$method]['price']+=($data['price']/100)*$structuredItem['price'];
						} else if (strtoupper($a[0])=="WEIGHTINC") {						
							$this->_request->setPackageWeight($this->_request->getPackageWeight() + $a[1]);
						} else if (strtoupper($a[0])=="PERCENTAGE_SHIP") {						
							$total[$method]['percentage'] += $data['price'];
						} else { // ORDER or PRODUCT						 
						 	$total[$method]['price'] += $data['price'];					 					
						}
					}	

					if ($data['rules']!="") {
						$rulesArr=explode("&",$data['rules']);  // Multi-formula extension
						foreach ($rulesArr as $ruleSingle) {
							$rule=explode("=",$ruleSingle,2);
							if (!empty($rule) && count($rule)==2) {
								if (strtolower($rule[0])=="w") {
									// weight based
									$weightIncrease=explode("@",$rule[1]);
									if (!empty($weightIncrease) && count($weightIncrease)==2 ) {
										$weightDifference=	$structuredItem['weight']-$data['weight_from_value'];
										$quotient=ceil($weightDifference / $weightIncrease[0]);
										$total[$method]['price']=$total[$method]['price']+$weightIncrease[1]*$quotient;
									}
								}
							}
						}
					}
					
				}
				return $total;
			}			
		}
		return ;
    }
    
	private function getStructuredItems($items, $conditionName)
	{
		$structuredItems=array();
		$useParent = Mage::getStoreConfig("carriers/handlingmatrix/bundle_id");
		
		$filterItem = $conditionName=='item' ? true : false;
		
		foreach($items as $item) {
						
			$weight=0;
			$qty=0;
			$price=0;
			$unique=0;
			
			$ignoreFreeShippingItems = !Mage::getStoreConfig('shipping/handlingmatrix/zero_shipping');

			if (!Mage::helper('wsacommon/shipping')->getItemTotals($item,$weight,$qty,$price,$useParent, $ignoreFreeShippingItems)) {  // dont ignore free items
				continue;
			}
			
			if ($item->getParentItem()!=null &&
				$useParent ) {
					// must be a bundle
					$product = Mage::getModel('catalog/product')->loadByAttribute('entity_id', $item->getParentItem()->getProductId(), 'handling_id');
					    
			} else if ($item->getProductType() == Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE && !$useParent ) {
				if ($item->getHasChildren()) {
                	foreach ($item->getChildren() as $child) {
                		$product = Mage::getModel('catalog/product')->loadByAttribute('entity_id', $child->getProductId(), 'handling_id');
						break;
                	}
				}
			} else {
				$product = Mage::getModel('catalog/product')->loadByAttribute('entity_id', $item->getProductId(), 'handling_id');
			}
			
			// if find a surcharge check to see if it is on the item or the order
			// if on the order then add to the surcharge_order_price if > than previous
			// if on the item then multiple by qty and add to the surcharge_price
			$handlingId = $product->getAttributeText('handling_id');
			$found=false;
			
			if (empty($handlingId)) { $handlingId ='none'; }
			if ($filterItem) {
				$prodArray=array(
						'handling_id'  	=> $handlingId,
						'price'			=> $price/$qty,
						'weight'		=> $weight/$qty,
			  			'qty' 			=> $qty,
						'unique'		=> 1);
				$structuredItems[]=$prodArray;
				
				if ($this->_starIncludeAll) {
					$prodArray['handling_id']='WSA_ALL';
					$structuredItems[]=$prodArray;
				}
			} else {
				foreach($structuredItems as $key=>$structuredItem) {
					if ($structuredItem['handling_id']==$handlingId) {
						// have already got this package id
						$structuredItems[$key]['qty']=$structuredItems[$key]['qty']+$qty;
						$structuredItems[$key]['price']=$structuredItems[$key]['price']+$price;
						$structuredItems[$key]['weight']=$structuredItems[$key]['weight']+$price;
						$structuredItems[$key]['unique']+=1;
						$found=true;
						break;
					}
				}
					
				if (!$found){
					$prodArray=array( 	'handling_id'   => $handlingId,
										'price'			=> $price,
										'weight'		=> $weight,
										'qty' 			=> $qty,
										'unique'		=> 1);
					$structuredItems[]=$prodArray;
					
				}
			}
			$unique++;
		}
		if ($this->_starIncludeAll && !$filterItem) {
			$prodArray=array( 	'handling_id'  => 'include_all',
									'price'		=> $this->_request->getPackageValue(),
									'weight'	=> $this->_request->getPackageWeight(),
									'qty' 		=> $this->_request->getPackageQty(),
									'unique'	=> $unique);
			$structuredItems[]=$prodArray;
		}
		if ($this->_debug) {
			Mage::helper('wsalogger/log')->postDebug('handlingmatrix','Structured Items',$structuredItems);				
		}
		return $structuredItems;
	}

	
	
	
	/**
	 * CSV Import routine
	 * @param $object
	 * @return unknown_type
	 */
    public function uploadAndImport(Varien_Object $object)
    {
        $csvFile = $_FILES["groups"]["tmp_name"]["handlingmatrix"]["fields"]["import"]["value"];
        $csvName = $_FILES["groups"]["name"]["handlingmatrix"]["fields"]["import"]["value"];

        if (!empty($csvFile)) {

            $csv = trim(file_get_contents($csvFile));

            $table = Mage::getSingleton('core/resource')->getTableName('handlingmatrix/handlingmatrix');

            $websiteId = $object->getScopeId();
            $websiteModel = Mage::app()->getWebsite($websiteId);

            Mage::helper('wsacommon/shipping')->saveCSV($csv,$csvName,$websiteId,'handlingmatrix');
            
            if (!empty($csv)) {
                $exceptions = array();
                $csvLines = explode("\n", $csv);
                $csvLine = array_shift($csvLines);
                $csvLine = $this->_getCsvValues($csvLine);
                if (count($csvLine) < 10) {
                    $exceptions[0] = Mage::helper('shipping')->__('Invalid Handling Matrix File Format');
                }

                $countryCodes = array();
                $regionCodes = array();
                foreach ($csvLines as $k=>$csvLine) {
                    $csvLine = $this->_getCsvValues($csvLine);
                    if (count($csvLine) > 0 && count($csvLine) < 10) {
                        $exceptions[0] = Mage::helper('shipping')->__('Invalid Handling Matrix File Format');
                    } else {
                        $splitCountries = explode(",", trim($csvLine[0]));
                    	foreach ($splitCountries as $country) {
                        	$countryCodes[] = trim($country);
                    	}
                    	$regionCodes[] = $csvLine[1];
                    }
                }
                
                
                if (empty($exceptions)) {
                    $data = array();
                    $countryCodesToIds = array();
                    $regionCodesToIds = array();
                    $countryCodesIso2 = array();

                    $countryCollection = Mage::getResourceModel('directory/country_collection')->addCountryCodeFilter($countryCodes)->load();
                    foreach ($countryCollection->getItems() as $country) {
                        $countryCodesToIds[$country->getData('iso3_code')] = $country->getData('country_id');
                        $countryCodesToIds[$country->getData('iso2_code')] = $country->getData('country_id');
                        $countryCodesIso2[] = $country->getData('iso2_code');
                    }

                    $regionCollection = Mage::getResourceModel('directory/region_collection')
                        ->addRegionCodeFilter($regionCodes)
                        ->addCountryFilter($countryCodesIso2)
                        ->load();                    
                 
                        
                    foreach ($regionCollection->getItems() as $region) {
                        $regionCodesToIds[$countryCodesToIds[$region->getData('country_id')]][$region->getData('code')] = $region->getData('region_id');
                    }
                        
                    foreach ($csvLines as $k=>$csvLine) {
                        $csvLine = $this->_getCsvValues($csvLine);
                        $splitCountries = explode(",", trim($csvLine[0]));
                        
                        foreach ($splitCountries as $country) {
                        
                        	$country=trim($country);
                        	
                        	if (empty($countryCodesToIds) || !array_key_exists($country, $countryCodesToIds)) {
	                        	$countryId = '0';
	                            if ($country != '*' && $country != '') {
	                                $exceptions[] = Mage::helper('shipping')->__('Invalid Country "%s" in the Row #%s', $country, ($k+1));
	                            }
	                        } else {
	                            $countryId = $countryCodesToIds[$country];
	                        }
	
	                        if (!isset($countryCodesToIds[$country])
	                            || !isset($regionCodesToIds[$countryCodesToIds[$country]])
	                            || !array_key_exists($csvLine[1], $regionCodesToIds[$countryCodesToIds[$country]])) {
	                            $regionId = '0';
	                            if ($csvLine[1] != '*' && $csvLine[1] != '') {
	                                $exceptions[] = Mage::helper('shipping')->__('Invalid Region/State "%s" in the Row #%s', $csvLine[1], ($k+1));
	                            }
	                        } else {
	                            $regionId = $regionCodesToIds[$countryCodesToIds[$country]][$csvLine[1]];
	                        }
	                        
							if ($csvLine[2] == '*' || $csvLine[2] == '') {
								$city = '';
							} else {
								$city = $csvLine[2];
							}
	
	
							if ($csvLine[3] == '*' || $csvLine[3] == '') {
								$zip = '';
							} else {
								$zip = $csvLine[3];
							}
	
	
							if ($csvLine[4] == '*' || $csvLine[4] == '') {
								$zip_to = '';
							} else {
								$zip_to = $csvLine[4];
							}
                        	if ($csvLine[5] == '*' || $csvLine[5] == '') {
								$handling_id = '';
							} else {
								$handling_id = $csvLine[5];
							}
							
							if (count($csvLine) == 16) { 
			                    if ( $csvLine[6] == '*' || $csvLine[6] == '') {
									$weight_from = -1;
								} else if (!is_numeric($csvLine[6])) {
									$exceptions[] = Mage::helper('shipping')->__('Invalid weight From "%s" in the Row #%s', $csvLine[6], ($k+1));
			                    } else {
									$weight_from = (float)$csvLine[6];
								}
			
								if ( $csvLine[7] == '*' || $csvLine[7] == '') {
									$weight_to = 10000000;
								} else if (!$this->_isPositiveDecimalNumber($csvLine[7])) {
									$exceptions[] = Mage::helper('shipping')->__('Invalid weight To "%s" in the Row #%s', $csvLine[7], ($k+1));
								}
								else {
									$weight_to = (float)$csvLine[7];
								}
								
								if ( $csvLine[8] == '*' || $csvLine[8] == '') {
									$price_from = -1;
								} else if (!$this->_isPositiveDecimalNumber($csvLine[8]) ) {
									$exceptions[] = Mage::helper('shipping')->__('Invalid price From "%s" in the Row #%s',  $csvLine[8], ($k+1));
								} else {
									$price_from = (float)$csvLine[8];
								}
		
								if ( $csvLine[9] == '*' || $csvLine[9] == '') {
									$price_to = 10000000;
								} else if (!$this->_isPositiveDecimalNumber($csvLine[9])) {
									$exceptions[] = Mage::helper('shipping')->__('Invalid price To "%s" in the Row #%s', $csvLine[9], ($k+1));
								} else {
									$price_to = (float)$csvLine[9];
								}
								
								
								if ( $csvLine[10] == '*' || $csvLine[10] == '') {
									$item_from = 0;
								} else if (!$this->_isPositiveDecimalNumber($csvLine[8]) ) {
									$exceptions[] = Mage::helper('shipping')->__('Invalid item From "%s" in the Row #%s',  $csvLine[10], ($k+1));
								} else {
									$item_from = (float)$csvLine[10];
								}
		
								if ( $csvLine[11] == '*' || $csvLine[11] == '') { 
									$item_to = 10000000;
								} else if (!$this->_isPositiveDecimalNumber($csvLine[11])) {
									$exceptions[] = Mage::helper('shipping')->__('Invalid item To "%s" in the Row #%s', $csvLine[11], ($k+1));
								} else {
									$item_to = (float)$csvLine[11];
								}
	
								if ( $csvLine[14] == '*' || $csvLine[14] == '') {
									$deliveryType = '';
								} else {
									$deliveryType = $csvLine[14];
								}
								
								if ( $csvLine[15] == '*' || $csvLine[15] == '') {
									$rules = '';
								} else {
									$rules = $csvLine[15];
								}

								$data[] = array('website_id'=>$websiteId, 'dest_country_id'=>$countryId, 'dest_region_id'=>$regionId,
									'dest_city'=>$city, 'dest_zip'=>$zip, 'dest_zip_to'=>$zip_to,
									'weight_from_value'=>$weight_from,'weight_to_value'=>$weight_to,
									'price_from_value'=>$price_from,'price_to_value'=>$price_to,
									'item_from_value'=>$item_from,'item_to_value'=>$item_to,
									'handling_id'=>$handling_id,
									'price'=>$csvLine[12], 'algorithm'=>$csvLine[13], 'delivery_type'=>$deliveryType,'rules'=>$rules);

								$dataDetails[] = array('country'=>$country, 'region'=>$csvLine[1]);
								
							} else {
							
	                        	if ( $csvLine[6] == '*' || $csvLine[6] == '') {
									$price_from = -1;
								} else if (!$this->_isPositiveDecimalNumber($csvLine[6]) ) {
									$exceptions[] = Mage::helper('shipping')->__('Invalid price From "%s" in the Row #%s',  $csvLine[6], ($k+1));
								} else {
									$price_from = (float)$csvLine[6];
								}
		
								if ( $csvLine[7] == '*' || $csvLine[7] == '') {
									$price_to = 10000000;
								} else if (!$this->_isPositiveDecimalNumber($csvLine[7])) {
									$exceptions[] = Mage::helper('shipping')->__('Invalid price To "%s" in the Row #%s', $csvLine[7], ($k+1));
								} else {
									$price_to = (float)$csvLine[7];
								}
								
								
								if ( $csvLine[8] == '*' || $csvLine[8] == '') {
									$item_from = 0;
								} else if (!$this->_isPositiveDecimalNumber($csvLine[8]) ) {
									$exceptions[] = Mage::helper('shipping')->__('Invalid item From "%s" in the Row #%s',  $csvLine[8], ($k+1));
								} else {
									$item_from = (float)$csvLine[8];
								}
		
								if ( $csvLine[9] == '*' || $csvLine[9] == '') { 
									$item_to = 10000000;
								} else if (!$this->_isPositiveDecimalNumber($csvLine[9])) {
									$exceptions[] = Mage::helper('shipping')->__('Invalid item To "%s" in the Row #%s', $csvLine[9], ($k+1));
								} else {
									$item_to = (float)$csvLine[9];
								}
								if (count($csvLine) == 13) {
	
									if ( $csvLine[12] == '*' || $csvLine[12] == '') {
										$deliveryType = '';
									} else {
										$deliveryType = $csvLine[12];
									}
	
									$data[] = array('website_id'=>$websiteId, 'dest_country_id'=>$countryId, 'dest_region_id'=>$regionId,
										'dest_city'=>$city, 'dest_zip'=>$zip, 'dest_zip_to'=>$zip_to,
										'price_from_value'=>$price_from,'price_to_value'=>$price_to,
										'item_from_value'=>$item_from,'item_to_value'=>$item_to,
										'handling_id'=>$handling_id,
										'price'=>$csvLine[10], 'algorithm'=>$csvLine[11], 'delivery_type'=>$deliveryType);
	
									$dataDetails[] = array('country'=>$country, 'region'=>$csvLine[1]);
																	
								} else {
									$data[] = array('website_id'=>$websiteId, 'dest_country_id'=>$countryId, 'dest_region_id'=>$regionId,
										'dest_city'=>$city, 'dest_zip'=>$zip, 'dest_zip_to'=>$zip_to,
										'price_from_value'=>$price_from,'price_to_value'=>$price_to,
										'item_from_value'=>$item_from,'item_to_value'=>$item_to,
										'handling_id'=>$handling_id,
										'price'=>$csvLine[10], 'algorithm'=>$csvLine[11]);
	
									$dataDetails[] = array('country'=>$country, 'region'=>$csvLine[1]);
								}
							}
                        }
                    }
                }
                if (empty($exceptions)) {
                    $connection = $this->_getWriteAdapter();

                     $condition = array(
                        $connection->quoteInto('website_id = ?', $websiteId),
                    );
                    $connection->delete($table, $condition);
                    

                    foreach($data as $k=>$dataLine) {
                        try {
                        	$connection->insert($table, $dataLine);
                        } catch (Exception $e) {
                            $exceptions[] = $e;
                        }
                    }
                }
                if (!empty($exceptions)) {
                    throw new Exception( "\n" . implode("\n", $exceptions) );
                }
            }
        }
    }

    private function _getCsvValues($string, $separator=",")
    {
        $elements = explode($separator, trim($string));
        for ($i = 0; $i < count($elements); $i++) {
            $nquotes = substr_count($elements[$i], '"');
            if ($nquotes %2 == 1) {
                for ($j = $i+1; $j < count($elements); $j++) {
                    if (substr_count($elements[$j], '"') > 0) {
                        // Put the quoted string's pieces back together again
                        array_splice($elements, $i, $j-$i+1, implode($separator, array_slice($elements, $i, $j-$i+1)));
                        break;
                    }
                }
            }
            if ($nquotes > 0) {
                // Remove first and last quotes, then merge pairs of quotes
                $qstr =& $elements[$i];
                $qstr = substr_replace($qstr, '', strpos($qstr, '"'), 1);
                $qstr = substr_replace($qstr, '', strrpos($qstr, '"'), 1);
                $qstr = str_replace('""', '"', $qstr);
            }
            $elements[$i] = trim($elements[$i]);
        }
        return $elements;
    }

    private function _isPositiveDecimalNumber($n)
    {
        return preg_match ("/^[0-9]+(\.[0-9]*)?$/", $n);
    }
    


}
