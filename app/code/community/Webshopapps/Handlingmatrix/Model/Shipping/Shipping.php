<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Mage
 * @package    Mage_Shipping
 * @copyright  Copyright (c) 2008 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


class Webshopapps_Handlingmatrix_Model_Shipping_Shipping extends Mage_Shipping_Model_Shipping
{
      public function collectCarrierRates($carrierCode, $request)
	  {
     	$handlingMatrixConfig=Mage::getStoreConfig('shipping/handlingmatrix');
	 
	    $handlingMatrixEnabled = false;
		if(isset($handlingMatrixConfig['active'])) {
			 $handlingMatrixEnabled = $handlingMatrixConfig['active'];
		}
     	if (!$handlingMatrixEnabled || sizeof($request->getAllItems())<1) { 
     		return parent::collectCarrierRates($carrierCode,$request);
     	}
		if (!Mage::helper('wsacommon')->checkItems('c2hpcHBpbmcvaGFuZGxpbmdtYXRyaXgvc2hpcF9vbmNl',
		'YmlnYmFuYW5hcw==','c2hpcHBpbmcvaGFuZGxpbmdtYXRyaXgvc2VyaWFs')) { 
			return parent::collectCarrierRates($carrierCode,$request); }
     	
      	$handlingMatrixModel = Mage::getModel('handlingmatrix/handlingmatrix');
     	
        $carrier = $this->getCarrierByCode($carrierCode, $request->getStoreId());
        if (!$carrier) {
            return $this;
        }
        $result = $carrier->checkAvailableShipCountries($request);
        if (false !== $result && !($result instanceof Mage_Shipping_Model_Rate_Result_Error)) {
            if (method_exists($carrier,'proccessAdditionalValidation')) {
        		$result = $carrier->proccessAdditionalValidation($request);
            }
        }
        /*
        * Result will be false if the admin set not to show the shipping module
        * if the devliery country is not within specific countries
        */
        if (false !== $result){
            if (!$result instanceof Mage_Shipping_Model_Rate_Result_Error) {
                $result = $carrier->collectRates($request);
				if (!$result) {
                    return $this;
                }
            }
			 if ($carrier->getConfigData('showmethod') == 0 && $result->getError()) {
	                return $this;
	            }
            $handlingMatrixModel->addHandlingCosts($request,$result);
            
            // sort rates by price
            if (method_exists($result, 'sortRatesByPrice')) {
                $result->sortRatesByPrice();
            }
            
            $this->getResult()->append($result);
            
        }
        return $this;
    }
}
