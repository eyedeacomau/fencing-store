<?php

$installer = $this;

$installer->startSetup();

$installer->run(" ALTER TABLE {$this->getTable('handlingmatrix')} ADD delivery_type varchar(255) NOT NULL default '',

DROP INDEX `dest_country`,

ADD UNIQUE `dest_country` (`website_id`,`dest_country_id`,`dest_region_id`,`dest_city`,`dest_zip`,`dest_zip_to`,`handling_id`,`price_from_value`,`price_to_value`,`item_from_value`,`item_to_value`,`algorithm`,`delivery_type`)

");

$installer->endSetup();


