<?php

$installer = $this;

$installer->startSetup();

$installer->run(" 

ALTER IGNORE TABLE {$this->getTable('handlingmatrix')}  
    ADD weight_from_value decimal(12,4) NULL default '-1.0000',
    ADD weight_to_value decimal(12,4) NULL default '1000000',
    DROP INDEX `dest_country`;
    
ALTER IGNORE TABLE {$this->getTable('handlingmatrix')} ADD rules varchar(255) NULL;
    

");

$installer->endSetup();


