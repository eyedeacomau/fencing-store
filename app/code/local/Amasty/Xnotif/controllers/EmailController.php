<?php
/**
* @author Amasty Team
* @copyright Copyright (c) 2012 Amasty (http://www.amasty.com)
* @package Amasty_Xnotif
*/   
class Amasty_Xnotif_EmailController extends Mage_Core_Controller_Front_Action
{
     public function stockAction()
    {
        $session = Mage::getSingleton('catalog/session');
        /* @var $session Mage_Catalog_Model_Session */
        $backUrl    = $this->getRequest()->getParam(Mage_Core_Controller_Front_Action::PARAM_NAME_URL_ENCODED);
        $productId  = (int) $this->getRequest()->getParam('product_id');
        $guestEmail  = $this->getRequest()->getParam('guest_email');
        $parentId  = (int) $this->getRequest()->getParam('parent_id');
         
        if (!$backUrl) {
            $this->_redirect('/');
            return ;
        }

        if (!$product = Mage::getModel('catalog/product')->load($productId)) {
            /* @var $product Mage_Catalog_Model_Product */
            $session->addError($this->__('Not enough parameters.'));
            $this->_redirectUrl($backUrl);
            return ;
        }
		//Online Vision
		$getProductExistingEmail = Mage::getModel('productalert/stock')
                    ->getCustomerCollection()
					->join($product->getId(), Mage::app()->getStore()->getWebsiteId());
		$email_array = array();
		foreach($getProductExistingEmail as $email){
			$email_array[]= $email['guest_email'];
		}
		if (in_array($guestEmail, $email_array)) {
			Mage::getSingleton('core/session')->addError('Email exist in alert subscription.');
			 $this->_redirectReferer();
            return ;
		}
		//OVEDIT EOF
        try {          
            $model = Mage::getModel('productalert/stock')
                ->setProductId($product->getId())
                ->setEmail($guestEmail)
                ->setWebsiteId(Mage::app()->getStore()->getWebsiteId());
           
            if ($parentId){
                 $model->setParentId($parentId);
            }
            $model->save();
            $session->addSuccess($this->__('Alert subscription has been saved.'));
			//Online Vision
			Mage::getSingleton('core/session')->addSuccess('Alert subscription has been saved.');
			//OVEDIT EOF
        }
        catch (Exception $e) {
            $session->addException($e, $this->__('Unable to update the alert subscription.'));
			//Online Vision
			Mage::getSingleton('core/session')->addError('Unable to update the alert subscription.');
			//OVEDIT EOF
        }
        $this->_redirectReferer();
    }
}