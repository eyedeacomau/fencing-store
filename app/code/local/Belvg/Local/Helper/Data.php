<?php
class Belvg_Local_Helper_Data extends Mage_Core_Helper_Data
{
    public function getStaticBlockTitle($identifier)
    {
        return Mage::getModel('cms/block')->load($identifier)->getTitle();
    }

    public function getStaticBlockHtml($identifier)
    {
        return Mage::app()->getLayout()->createBlock('cms/block')->setBlockId($identifier)->toHtml();
    }


}