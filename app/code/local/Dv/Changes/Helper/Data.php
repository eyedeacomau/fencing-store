<?php

class Dv_Changes_Helper_Data extends Mage_Core_Helper_Abstract
{

    //need remove this hardcode
    protected $_deliveryText = 'OK to leave if no-one available to sign';

    public function getDeliverySignAttributeText($order = null){
        if (is_null($order)) {
            $order = Mage::registry('sales_order');
        }
        if (!$order) {
            return '';
        }
        $result = $this->_deliveryText . ': <b>';
        if ($deliverySignText = $order->custom('delivery_sign')){
            if (strpos($deliverySignText, $this->_deliveryText) !== false){
                $result .= $this->__('Yes');
            } else {
                $result .= $this->__('No');
            }
        } else {
            $result .= $this->__('No');
        }
        return $result . '</b>';
    }

    public function getSpecialDeliveryInstructionsAttributeText($order = null){
        if (is_null($order)) {
            $order = Mage::registry('sales_order');
        }
        if (!$order) {
            return '';
        }

        if ($deliverySignText = $order->custom('special_delivery_instructions')){
            return nl2br($deliverySignText);
        }
        return '';
    }

}