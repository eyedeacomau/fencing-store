<?php

require_once 'Mage/Catalog/controllers/ProductController.php';


class Dv_Changes_ProductController extends Mage_Catalog_ProductController
{

    public function viewAction()
    {
        $rewriteRequest = pathinfo($this->getRequest()->getAlias('rewrite_request_path'));
        $categoryId = (int) $this->getRequest()->getParam('category', false);
        if (!Mage::getStoreConfig(Mage_Catalog_Helper_Product::XML_PATH_PRODUCT_URL_USE_CATEGORY, Mage::app()->getStore()->getStoreId())
            && $categoryId
            && isset($rewriteRequest['basename'])
        ) {
            $this->_redirectUrl(Mage::getBaseUrl() . '' . $rewriteRequest['basename']);
            return;
        } else {
            parent::viewAction();
        }
    }

}