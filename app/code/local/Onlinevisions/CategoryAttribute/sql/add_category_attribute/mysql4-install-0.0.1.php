<?php
$this->startSetup();
$this->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'custom_category_category_links', array(
    'group'         => 'General Information',
    'input'         => 'textarea',
    'type'          => 'text',
    'label'         => 'Category Links',
    'backend'       => '',
    'visible'       => true,
    'required'      => false,
    'visible_on_front' => true,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
));
$this->updateAttribute(Mage_Catalog_Model_Category::ENTITY, 'custom_category_category_links', 'is_wysiwyg_enabled', 1);
$this->updateAttribute(Mage_Catalog_Model_Category::ENTITY, 'custom_category_category_links', 'is_html_allowed_on_front', 1);
$this->endSetup();