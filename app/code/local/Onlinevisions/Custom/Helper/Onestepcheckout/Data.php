<?php

class Onlinevisions_Custom_Helper_Onestepcheckout_Data extends Magestore_Onestepcheckout_Helper_Data
{
    public function savePaymentMethod($data)
    {
        if (empty($data)) {
            return array('error' => -1, 'message' => Mage::helper('checkout')->__('Invalid data.'));
        }
        $onepage = Mage::getSingleton('checkout/session')->getQuote();
        if ($onepage->isVirtual()) {
            $onepage->getBillingAddress()->setPaymentMethod(isset($data['method']) ? $data['method'] : null);
        } else {
            $onepage->getShippingAddress()->setPaymentMethod(isset($data['method']) ? $data['method'] : null);
        }
        $payment = $onepage->getPayment();
        $payment->importData($data);

        $onepage->setTotalsCollectedFlag(false)->collectTotals()->save();

        return array();
    }
}