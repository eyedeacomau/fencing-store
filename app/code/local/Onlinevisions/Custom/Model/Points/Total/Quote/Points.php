<?php

class Onlinevisions_Custom_Model_Points_Total_Quote_Points extends AW_Points_Model_Total_Quote_Points
{
    public function collect(Mage_Sales_Model_Quote_Address $address)
    {
        $quote = $address->getQuote();
        $session = Mage::getSingleton('checkout/session');
        if (is_null($session->getQuoteId())) {
            $session = Mage::getSingleton('adminhtml/session_quote');
        }
        $isCustomerLoggedIn = (bool)$quote->getCustomer()->getId();

        if ($session->getData('use_points') && $address->getBaseGrandTotal() && $isCustomerLoggedIn) {
            $customer = Mage::getModel('customer/customer')->load($quote->getCustomer()->getId());
            $pointsAmountUsed = abs($session->getData('points_amount'));
            $pointsAmountAllowed = Mage::getModel('points/summary')
                ->loadByCustomer($customer)
                ->getPoints()
            ;

            $storeId = $session->getQuote()->getStoreId();
            $website = Mage::app()->getWebsite(Mage::app()->getStore($storeId)->getWebsiteId());

            $baseSubtotalWithDiscount = $address->getData('base_subtotal') + $address->getData('base_discount_amount');
            $subtotalWithDiscount = $address->getData('subtotal') + $address->getData('discount_amount');

            $pointsSpendingCalculation = Mage::helper('points/config')->getPointsSpendingCalculation();
            if ($pointsSpendingCalculation !== AW_Points_Helper_Config::BEFORE_TAX) {
                $baseSubtotalWithDiscount += $address->getData('base_tax_amount');
                $subtotalWithDiscount += $address->getData('tax_amount');
            }

            $limitedPoints = Mage::helper('points')->getLimitedPoints($baseSubtotalWithDiscount, $customer, $storeId);
            $pointsAmountUsed = min($pointsAmountUsed, $pointsAmountAllowed, $limitedPoints);

            $session->setData('points_amount', $pointsAmountUsed);
            $rate = Mage::getModel('points/rate')
                ->setCurrentCustomer($customer)
                ->setCurrentWebsite($website)
                ->loadByDirection(AW_Points_Model_Rate::POINTS_TO_CURRENCY)
            ;

            $moneyBaseCurrencyForPoints = $rate->exchange($pointsAmountUsed);
            $moneyCurrentCurrencyForPoints = Mage::app()->getStore()->convertPrice($moneyBaseCurrencyForPoints);
            /**
             * If points amount is more then needed to pay for subtotal with discount for order,
             * we need to set new points amount
             */
            if ($moneyBaseCurrencyForPoints > $baseSubtotalWithDiscount) {
                $neededAmount = ceil($baseSubtotalWithDiscount * $rate->getPoints() / $rate->getMoney());
                $neededAmountBaseCurrency = $rate->exchange($neededAmount);
                $neededAmountCurrentCurrency = Mage::app()->getStore()->convertPrice($neededAmountBaseCurrency);
                $session->setData('points_amount', $neededAmount);
                $address->setGrandTotal($address->getData('grand_total') - $subtotalWithDiscount);
                $address->setBaseGrandTotal($address->getData('base_grand_total') - $baseSubtotalWithDiscount);
                $address->setMoneyForPoints($neededAmountCurrentCurrency);
                $address->setBaseMoneyForPoints($neededAmountBaseCurrency);
                $quote->setMoneyForPoints($neededAmountCurrentCurrency);
                $quote->setBaseMoneyForPoints($neededAmountBaseCurrency);
            } else {
                $address->setGrandTotal($address->getGrandTotal() - $moneyCurrentCurrencyForPoints);
                $address->setBaseGrandTotal($address->getBaseGrandTotal() - $moneyBaseCurrencyForPoints);
                $address->setMoneyForPoints($moneyCurrentCurrencyForPoints);
                $address->setBaseMoneyForPoints($moneyBaseCurrencyForPoints);
                $quote->setMoneyForPoints($moneyCurrentCurrencyForPoints);
                $quote->setBaseMoneyForPoints($moneyBaseCurrencyForPoints);
            }
        } else {
            $address->setMoneyForPoints(0);
            $address->setBaseMoneyForPoints(0);
            $quote->setMoneyForPoints(0);
            $quote->setBaseMoneyForPoints(0);
        }
        return $this;
    }
}