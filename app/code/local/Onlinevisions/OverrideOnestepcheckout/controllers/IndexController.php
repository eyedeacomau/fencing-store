<?php
require_once 'Magestore/Onestepcheckout/controllers/IndexController.php';
class Onlinevisions_OverrideOnestepcheckout_IndexController extends Magestore_Onestepcheckout_IndexController {

	/*
	* save billing & shipping address
	*/
	public function save_addressAction() {
		$billing_data = $this->getRequest()->getPost('billing', false);
		$shipping_data = $this->getRequest()->getPost('shipping', false);
		$shipping_method = $this->getRequest()->getPost('shipping_method', false);
		$billing_address_id = $this->getRequest()->getPost('billing_address_id', false);

		//load default data for disabled fields
		if (Mage::helper('onestepcheckout')->isUseDefaultDataforDisabledFields()) {
			Mage::helper('onestepcheckout')->setDefaultDataforDisabledFields($billing_data);
			Mage::helper('onestepcheckout')->setDefaultDataforDisabledFields($shipping_data);
		}

		if (isset($billing_data['use_for_shipping']) && $billing_data['use_for_shipping'] == '1') {
			$shipping_address_data = $billing_data;
		}
		else {
			$shipping_address_data = $shipping_data;
		}

		$billing_street = trim(implode("\n", $billing_data['street']));
		$shipping_street = trim(implode("\n", $shipping_address_data['street']));

		if(isset($billing_data['email'])) {
			$billing_data['email'] = trim($billing_data['email']);
		}

		// Ignore disable fields validation --- Only for 1..4.1.1
		$this->setIgnoreValidation();
		if(Mage::helper('onestepcheckout')->isShowShippingAddress()) {
			if(!isset($billing_data['use_for_shipping']) || $billing_data['use_for_shipping'] != '1')	{
				$shipping_address_id = $this->getRequest()->getPost('shipping_address_id', false);
				$this->getOnepage()->saveShipping($shipping_data, $shipping_address_id);
			}else{
				$this->getOnepage()->saveShipping($billing_data, $billing_address_id);
			}
		}
		$this->getOnepage()->saveBilling($billing_data, $billing_address_id);
		if($billing_data['country_id']){
			Mage::getModel('checkout/session')->getQuote()->getBillingAddress()->setData('country_id',$billing_data['country_id'])->save();
		}
		//var_dump($billing_data['country_id']);die();
		//Mage::getModel('core/session')->setData('country',$billing_data['country_id']);
		// if different shipping address is enabled and customer ship to another address, save it


		if ($shipping_method && $shipping_method != '') {
			Mage::helper('onestepcheckout')->saveShippingMethod($shipping_method);
		}
		$this->loadLayout(false);
		$this->renderLayout();
	}
}