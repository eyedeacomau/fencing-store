<?php

class Onlinevisions_OverrideSupplierExtended_Model_Output extends Magentomasters_Supplier_Model_Output
{
		protected function processTemplate($order,$supplier,$items,$type){
            $templates = $this->getTemplates($supplier,null);
            $settings = $this->getSettings($order->getStoreId());
            $customer = Mage::getModel('customer/customer')->load($order->getCustomerId());
            $customer = $customer->getData();
            foreach($customer as $key => $value ){ $customerarray['customer_' . $key] = $value; }

            if($order->getIsVirtual()!=1){
                $shippingaddress = $order->getShippingAddress()->getData();
                foreach($shippingaddress as $key => $value ){ $shippingarray['shipping_' . $key] = $value; }
            }

            $billingaddress = $order->getBillingAddress()->getData();
            foreach($billingaddress as $key => $value ){ $billingarray['billing_' . $key] = $value; }

            if($supplier['email_header']){
                $emailHeader = $supplier['email_header'];
            } else {
                $emailHeader = $settings['message_header'];
            }

            if($supplier['email_message']){
                $emailMessage = $supplier['email_message'];
            } else {
                $emailMessage = $settings['message_for_supplier'];
            }

            if($supplier['pdf_header']){
                $pdfHeader = $supplier['pdf_header'];
            } else {
                $pdfHeader = $settings['pdf_header'];
            }

            if($supplier['pdf_message']){
                $pdfMessage = $supplier['pdf_message'];
            } else {
                $pdfMessage = $settings['pdf_for_supplier'];
            }

            if(isset($settings['filename'])){
                $supplierImage = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB) . 'media/supplier/' . $settings['filename'];
            } else {
                $supplierImage = '';
            }

            if(isset($settings['pdffilename'])){
                $supplierImagePDF = '<img src="media/supplier/' . $settings['pdffilename'] . '"/>';
            } else {
                $supplierImagePDF = '&nbsp;';
            }


            $templatePDFitems_processed = $this->processitemTemplate($templates['pdfitems'],$items,$type,null,$supplier);
            $templateEmailitems_processed = $this->processitemTemplate($templates['emailitems'],$items,$type,null,$supplier);
            $templateXMLitems_processed = $this->processitemTemplate($templates['xmlitems'],$items,$type,null,$supplier);

            if($order->getGiftMessageId()){
                $giftmessage = Mage::getModel('giftmessage/message')->load($order->getGiftMessageId())->getData();
                $giftmessagearray = array();
                foreach($giftmessage as $key=>$value) {
                    $giftmessagearray['order_giftmessage_'.$key] = $value;
                }
            }

            //
            $confirmationLink = '';
            $completedLink = '';
            //
            if (isset($supplier['name']) && isset($supplier['password'])) {
                $confirmationLink = Mage::getUrl(
                    'supplier/index/confirm',
                    array(
                        'user' => $supplier['name'],
                        'token' => substr($supplier['password'], 0, 16) . '.' . md5($supplier['id']),
                        'order' => $order->getId(),
                    )
                );
                $completedLink = Mage::getUrl(
                    'supplier/index/complete',
                    array(
                        'user' => $supplier['name'],
                        'token' => substr($supplier['password'], 0, 16) . '.' . md5($supplier['id']),
                        'order' => $order->getId(),
                    )
                );
            }

            $variables = array(
                'order' => $order,
                'logo_pdf' =>  $supplierImagePDF,
                'logo_email' => $supplierImage,
                'supplier' => $supplier,
                'dropship_date' => date('D, d M Y H:i:s', time()),
                'email_header' => $emailHeader,
                'email_message' => $emailMessage,
                'email_items'=> $templateEmailitems_processed,
                'pdf_header' => $pdfHeader,
                'pdf_message' => $pdfMessage,
                'pdf_items'=> $templatePDFitems_processed,
                'xml_items'=> $templateXMLitems_processed,
                'total_cost' => Mage::helper('core')->currency($this->getTotalCosts($items), true, false),
                'confirmation_link' => $confirmationLink,
                'completed_link' => $completedLink,
            );

            if(!empty($shippingarray)){
                $addresses = array_merge($shippingarray, $billingarray);
            } else {
                $addresses = $billingarray;
            }

            $variables = array_merge($variables, $addresses );

            if(!empty($giftmessagearray)){ $variables = array_merge($variables, $giftmessagearray);}
            if(!empty($customerarray)){ $variables = array_merge($variables, $customerarray); }

            foreach($supplier as $key => $value ){ $variables['supplier_' . $key] = $value; }

            $processor = Mage::helper('cms')->getBlockTemplateProcessor();

            if($type=='pdf'){
                $processor->setVariables($variables);
                $finaltemplate = $processor->filter($templates['pdf']);
            } elseif($type=='email') {
                $processor->setVariables($variables);
                $finaltemplate = $processor->filter($templates['email']);
            } elseif($type=='xml'){
                $processor->setVariables($variables);
                $finaltemplate = $processor->filter($templates['xml']);
            } elseif($type=='xmlnew'){
                $xml_header = '<?xml version="1.0" encoding="UTF-8"?>';
                $xml_open= '<orderxml>';
                $xml_close = '</orderxml>';
                $xml_filter_options = array(
                    'xml_header' => $xml_header,
                    'xml_open' => $xml_open,
                    'xml_close' => $xml_close
                    );
                $variables = array_merge($variables, $xml_filter_options);
                $processor->setVariables($variables);
                $finaltemplate = $processor->filter($templates['xml']);
            } elseif($type=='csv'){
                $finaltemplate = $this->processitemTemplate($templates['csvitems'],$items,$type,$variables,$supplier);
            }
            return $finaltemplate;

        }

    public function getPdf($order_id, $supplier_id, $items) {
        $order = Mage::getModel('sales/order')->load($order_id);

        if (!Mage::registry('sales_order')) {
            Mage::register('sales_order', $order);
        }
        if (!Mage::registry('current_order')) {
            Mage::register('current_order', $order);
        }
        if (!Mage::registry('order')) {
            Mage::register('order', $order);
        }

        $supplier = Mage::getModel('supplier/supplier')->load($supplier_id)->getData();
        $settings = $this->getSettings($order->getStoreId());
        $finalpdf = $this->processTemplate($order,$supplier,$items,'pdf');
        if(!$finalpdf){ $finalpdf = 'No Template Found'; }
        Mage::getModel('supplier/observer')->logging($finalpdf);
        require_once 'includes/tcpdf/tcpdf.php';
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        $pdf->AddPage();
        $pdf->writeHTML($finalpdf, true, false, true, false);
        $pdf->lastPage();
        return $pdf->Output(false, 'S');
    }

    public function getEmail($order_id, $supplier_id, $items, $trigger){

        Sohi_Debug::log('Magentomasters_Supplier_Model_Output::getEmail::START');

        $order = Mage::getModel('sales/order')->load($order_id);
        if (!Mage::registry('sales_order')){
            Mage::register('sales_order', $order);
        }
        if (!Mage::registry('current_order')){
            Mage::register('current_order', $order);
        }
        if (!Mage::registry('order')){
            Mage::register('order', $order);
        }
        $supplier = Mage::getModel('supplier/supplier')->load($supplier_id)->getData();
        $settings = $this->getSettings($order->getStoreId());

        Mage::getModel('supplier/observer')->logging($supplier);

        $finalemail = $this->processTemplate($order,$supplier,$items,'email');
        Sohi_Debug::log($finalemail, '$finalemail');

        $mail = new Zend_Mail();
        $mail->addTo($supplier['email1'], $supplier['name']);
        if ($supplier['email2']){
            $mail->addTo($supplier['email2'], $supplier['name']);
        }
        if ($settings['bcc_address_email']){
            $mail->addBcc($settings['bcc_address_email']);
        }
        $mail->setBodyHtml($finalemail,'utf-8');

        // PDF attachement
        if ($supplier['pdf_enabled'] == 1){
            $orderIncrementId = $order->getRealOrderId();
            $at = $mail->createAttachment($this->getPdf($order_id,$supplier_id,$items));
            $at->type        = 'application/pdf';
            $at->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
            $at->encoding = Zend_Mime::ENCODING_BASE64;
            $at->filename    = "order_" . $orderIncrementId . ".pdf";
        }

        // CSV attachement
        if($supplier['email_attachement'] == 2 && $supplier['xml_enabled'] == 1 && $supplier['xml_csv'] == 1){
            Mage::getModel('supplier/observer')->logging("CSV Attachement Enabled");
            $file = $this->getCsv($order_id, $supplier_id, $items, $trigger);
            if($file){
                $fileContents = file_get_contents($file);
                $at2= $mail->createAttachment($fileContents);
                $at2->filename = $orderIncrementId . ".csv";
            } else {
                Mage::getModel('supplier/observer')->logging("CSV for email attachement Not found");
            }
        }

        // XML attachement
        if($supplier['email_attachement'] == 1 && $supplier['xml_enabled'] == 1 && $supplier['xml_csv'] == 0){
            Mage::getModel('supplier/observer')->logging("XML Attachement Enabled");
            $file = $this->getXML($order_id, $supplier_id, $items, $trigger);
            if($file){
                $fileContents = file_get_contents($file);
                $at3 = $mail->createAttachment($fileContents);
                $at3->filename = $orderIncrementId . ".xml";
            } else {
                Mage::getModel('supplier/observer')->logging("XML for email attachement Not found");
            }
        }

        if(isset($supplier['email_subject']) && $supplier['email_subject']){
            $subject = $supplier['email_subject'];
        } else {
            $subject = $settings['email_subject'];
        }

        $subject .= ' Order #' . $order->getIncrementId();

        $senderName  = Mage::getStoreConfig('supplier/emailoptions/email_sender_name');
        $senderEmail = Mage::getStoreConfig('supplier/emailoptions/email_sender_email');
        $mail->setFrom($senderEmail, $senderName);
        $mail->setSubject($this->processSubject($subject, $order));
        if(isset($settings['smtp_enabled']) && $settings['smtp_enabled']==1){
            $config = $this->getSmtp($order->getStoreId());
            $transport = new Zend_Mail_Transport_Smtp($settings['smtp_server'], $config);
            $mailFlag = $mail->send($transport);
        } else {
            /*$helper = Mage::helper('ebizmarts_mandrill');
            //Check if should use Mandrill Transactional Email Service
            if(FALSE === $helper->useTransactionalService()){
                $mailFlag = $mail->send();
            } else {
                $message = $this->_prepareMessage($mail, $supplier, $settings);
                $message['html'] = $finalemail;
                try {
                    $sent = Mage::getModel('ebizmarts_mandrill/email_template')->getMail()->send($message);
                    if($sent->errorCode){
                        return false;
                    }

                }catch (Exception $e) {
                    Mage::logException($e);
                    return false;
                }
            }*/
            $mailFlag = $mail->send();
        }
        return true;
    }

    protected function _prepareMessage($zendMail,$supplier, $settings){

        $_toEmails = array($supplier['email1']);
        $_toNames = array($supplier['name']);
        if ($supplier['email2']){
            $_toEmails[] = $supplier['email2'];
            $_toNames[] = $supplier['name'];
        }
        if ($settings['bcc_address_email']){
            $_toBbc = $settings['bcc_address_email'];
        }else{
            $_toBbc = '';
        }

        $message = array (
            'subject'     => $zendMail->getSubject(),
            'from_name'   => Mage::getStoreConfig('supplier/emailoptions/email_sender_name'),
            'from_email'  => $zendMail->getFrom(),
            'to_email'    => $_toEmails,
            'to_name'     => $_toNames,
            'bcc_address' => $_toBbc,
            'headers'	  => array('Reply-To' => $zendMail->getReplyTo()),
            'html'        => $zendMail->getBodyHtml(true)
        );

        return $message;
    }

}
