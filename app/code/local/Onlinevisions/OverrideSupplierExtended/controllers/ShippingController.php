<?php

require_once 'Polcode/SupplierExtended/controllers/ShippingController.php';


class Onlinevisions_OverrideSupplierExtended_ShippingController extends Polcode_SupplierExtended_ShippingController
{

    public function completeAndShip($orderId,$itemsQty,$tracking){
        $email = true; // <-- Must be users email address  $order->getCustomerEmail()
        /*$carrier = 'custom';*/
        $includeComment = false;
        $comment = "The order is shipped by the supplier";
        $order = Mage::getModel('sales/order')->load($orderId);
        $convertor = Mage::getModel('sales/convert_order');
        $shipment = $convertor->toShipment($order);

        foreach ($order->getAllItems() as $k=>$orderItem) {

            if (!$orderItem->getQtyToShip()) {
                continue;
            }
            if ($orderItem->getIsVirtual()) {
                continue;
            }

            $item = $convertor->itemToShipmentItem($orderItem);

            $productId = $orderItem->getProductId();

            if($itemsQty[$productId]) {
                $item->setQty($itemsQty[$productId]);
                $shipment->addItem($item);
            }
        }

        $carrierTitle = NULL;

        /*if ($carrier == 'custom') {
            $carrierTitle = 'Playtimes';
        }*/
        foreach ($tracking as $_key => $data) {
            if ($_key == '__index__') continue;
            if (isset($data['carrier'])){
                $carrier = $data['carrier'] ? $data['carrier'] : $carrier;
            }
            $track = Mage::getModel('sales/order_shipment_track')->addData($data);
            //Online Vision
            $CarrierCode =  $data['carrier_code'];
            if ($CarrierCode != 'custom') {
                $carrier = Mage::getSingleton('shipping/config')->getCarrierInstance($CarrierCode);
                if($carrier){
                    $carrierTitle = $carrier->getConfigData('title');
                }else{
                    $carrierTitle = $carrier;
                }
                //OVEDIT eof
            }
            //OVEDIT eof
            $shipment->addTrack($track);
        }

        $shipment->register();
        $shipment->addComment($comment, $email && $includeComment);
        $shipment->setEmailSent(true);
        $shipment->getOrder()->setIsInProcess(true);

        $transactionSave = Mage::getModel('core/resource_transaction')
            ->addObject($shipment)
            ->addObject($shipment->getOrder())
            ->save();

        $shipment->sendEmail($email, ($includeComment ? $comment : ''));

        if ($shipment->getOrder()->canShip() && Mage::getBlockSingleton('supplier/header')->canShip($shipment->getOrder()->getId())) {
//            $_block = Mage::getBlockSingleton('supplier/header');

            $shipment->getOrder()->setStatus('part_shipped');
            $shipment->getOrder()->addStatusToHistory($order->getStatus(), 'Order is Part Shipped', false);
            $this->_partShipped = true;

        } else {
            $order->setStatus('Complete');
            $order->addStatusToHistory($order->getStatus(), 'Order Completed because every item have been shipped', false);
            $this->_partShipped = false;
        }
        $transactionSave = Mage::getModel('core/resource_transaction')
            ->addObject($shipment)
            ->addObject($shipment->getOrder())
            ->save();


    }

}