<?php
class Onlinevisions_ShippingTransportCompany_Helper_Data extends Mage_Core_Helper_Abstract
{

    protected static $totalShippingTransportCompany = 5;

    public function status()
    {
        if(mage::getStoreConfig('onlinevisions_shippingtransportcompany/general/allow_shipping_details') == 1){
            return true;
        }else{
            return false;
        } 
    }

    public function getShippingTransportCompanyName($carrierCode)
    {
        return mage::getStoreConfig('onlinevisions_shippingtransportcompany/'.$carrierCode.'/company_name');
    }

    public function getShippingTransportCompanyContact($carrierCode)
    {
        return mage::getStoreConfig('onlinevisions_shippingtransportcompany/'.$carrierCode.'/company_contact');
    }
    public function getShippingTransportCompanyWebsite($carrierCode)
    {
        return mage::getStoreConfig('onlinevisions_shippingtransportcompany/'.$carrierCode.'/company_website');
    }

    public function getShippingTransportCompanyEmail($carrierCode)
    {
        return mage::getStoreConfig('onlinevisions_shippingtransportcompany/'.$carrierCode.'/company_email');
    }

    public function getShippingTransportCompanies()
    {
        $totalShippingTransportCompany = self::$totalShippingTransportCompany;
        $customShippingTransportCompany = array();
        for ($i=1;$i<=$totalShippingTransportCompany;$i++) {
            if(mage::getStoreConfig('onlinevisions_shippingtransportcompany/shipping_company_'.$i.'/company_name')!=''){
                $customShippingTransportCompany['shipping_company_'.$i] = mage::getStoreConfig('onlinevisions_shippingtransportcompany/shipping_company_'.$i.'/company_name');
            }
        }
        return $customShippingTransportCompany;
    }

}