<?php
class Onlinevisions_ShippingTransportCompany_Model_ShippingMethod extends Mage_Core_Model_Abstract
{

    public function getAllOptions()
    {
        if (!$this->_options) {
            $methods = Mage::getSingleton('shipping/config')->getActiveCarriers();
            $options = array();
            foreach($methods as $_ccode => $_carrier)
            {
                $_methodOptions = array();
                if($_methods = $_carrier->getAllowedMethods())
                {
                    foreach($_methods as $_mcode => $_method)
                    {
                        $_code = $_ccode . '_' . $_mcode;
                        $_methodOptions[] = array('value' => $_code, 'label' => $_method);
                    }

                    if(!$_title = Mage::getStoreConfig("carriers/$_ccode/title"))
                        $_title = $_ccode;

                    $options[] = array(
                        'value' => $_methodOptions,
                        'label' => $code.' : '.$_title,
                    );
                }
            }
            $this->_options = $options;
        }

        return $this->_options;
    }

    public function toOptionArray()
    {
        return $this->getAllOptions();
    }
}