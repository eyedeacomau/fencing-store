<?php
/**
 * Backend model for import addresses from CSV
 *
 */

class Onlinevisions_SuburbSelector_Model_Adminhtml_System_Config_Backend_Import extends Mage_Core_Model_Config_Data
{
    public function _afterSave()
    {
        Mage::getResourceModel('ovsuburbselector/suburb')->uploadAndImport($this);
    }
}
