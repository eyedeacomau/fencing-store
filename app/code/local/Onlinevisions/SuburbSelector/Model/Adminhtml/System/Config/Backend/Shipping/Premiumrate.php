<?php

class Onlinevisions_SuburbSelector_Model_Adminhtml_System_Config_Backend_Shipping_Premiumrate extends Mage_Core_Model_Config_Data
{
    public function _afterSave()
    {
		Mage::getResourceModel('ovsuburbselector/premiumrate_carrier_premiumrate')->uploadAndImport($this);
    }
}
