<?php

class Onlinevisions_SuburbSelector_Model_Resource_Premiumrate_Carrier_Premiumrate extends Webshopapps_Premiumrate_Model_Mysql4_Carrier_Premiumrate
{

    protected $_fields = array(
        'dest_country_id',      //0
        'dest_region_id',       //1
        'dest_city',            //2
        'zone',                 //3
        'weight_from_value',    //4
        'weight_to_value',      //5
        'price_from_value',     //6
        'price_to_value',       //7
        'item_from_value',      //8
        'item_to_value',        //9
        'price',                //10
        'algorithm',            //11
        'delivery_type',        //12
        'sort_order'            //13
    );

    private $_debug;

    protected $_postcodesStatesMap = array(
        '0' => 'NT',
        '2' => 'NSW',
        '3' => 'VIC',
        '4' => 'QLD',
        '5' => 'SA',
        '6' => 'WA',
        '7' => 'TAS',
    );

    public function uploadAndImport(Varien_Object $object){
        $csvFile = $_FILES["groups"]["tmp_name"]["premiumrate"]["fields"]["import_by_zone"]["value"];
        $csvName = $_FILES["groups"]["name"]["premiumrate"]["fields"]["import_by_zone"]["value"];
        $session = Mage::getSingleton('adminhtml/session');
        $dataStored = false;

        if (!empty($csvFile)) {

            $csv = trim(file_get_contents($csvFile));

            $table = Mage::getSingleton('core/resource')->getTableName('premiumrate_shipping/premiumrate');

            $websiteId = $object->getScopeId();
            $websiteModel = Mage::app()->getWebsite($websiteId);

            Mage::helper('wsacommon/shipping')->saveCSV($csv, '_byzones_' . $csvName, $websiteId, 'premiumrate');

            /*
            getting condition name from post instead of the following commented logic
            */

            if (isset($_POST['groups']['premiumrate']['fields']['condition_name']['inherit'])) {
                $conditionName = (string)Mage::getConfig()->getNode('default/carriers/premiumrate/condition_name');
            } else {
                $conditionName = $_POST['groups']['premiumrate']['fields']['condition_name']['value'];
            }

            $conditionFullName = Mage::getModel('premiumrate_shipping/carrier_premiumrate')->getCode('condition_name_short', $conditionName);

            if (!empty($csv)) {
                $exceptions = array();
                $csvLines = explode("\n", $csv);
                $csvLine = array_shift($csvLines);
                $csvLine = $this->_getCsvValues($csvLine);
                if (count($csvLine) < 14) {
                    $exceptions[0] = Mage::helper('shipping')->__('Invalid Premium Matrix Rates File Format');
                }

                $countryCodes = array();
                $regionCodes = array();
                foreach ($csvLines as $k=>$csvLine) {
                    $csvLine = $this->_getCsvValues($csvLine);
                    if (count($csvLine) > 0 && count($csvLine) < 14) {
                        $exceptions[0] = Mage::helper('shipping')->__('Invalid Premium Matrix Rates by Zones File Format');
                    } else {
                        $splitCountries = explode(",", trim($csvLine[0]));
                        $splitRegions = explode(",", trim($csvLine[1]));
                        foreach ($splitCountries as $country) {
                            if (!in_array($country,$countryCodes)) {
                                $countryCodes[] = trim($country);
                            }
                        }
                        foreach ($splitRegions as $region) {
                            $regionCodes[] = $region;
                        }
                    }
                }
            }

            if (empty($exceptions)) {
                $connection = $this->_getWriteAdapter();

                $condition = array(
                    $connection->quoteInto('website_id = ?', $websiteId),
                    $connection->quoteInto('condition_name = ?', $conditionName),
                );
                $connection->delete($table, $condition);


            }
            if (!empty($exceptions)) {
                throw new Exception( "\n" . implode("\n", $exceptions) );
            }



            if (empty($exceptions)) {
                $data = array();
                $countryCodesToIds = array();
                $regionCodesToIds = array();
                $countryCodesIso2 = array();
                $counter = 0;
                $countryCollection = Mage::getResourceModel('directory/country_collection')->addCountryCodeFilter($countryCodes)->load();
                foreach ($countryCollection->getItems() as $country) {
                    $countryCodesToIds[$country->getData('iso3_code')] = $country->getData('country_id');
                    $countryCodesToIds[$country->getData('iso2_code')] = $country->getData('country_id');
                    $countryCodesIso2[] = $country->getData('iso2_code');
                }

                $regionCollection = Mage::getResourceModel('directory/region_collection')
                    ->addRegionCodeFilter($regionCodes)
                    ->addCountryFilter($countryCodesIso2)
                    ->load();

                foreach ($regionCollection->getItems() as $region) {
                    $regionCodesToIds[$countryCodesToIds[$region->getData('country_id')]][$region->getData('code')] = $region->getData('region_id');
                }

                foreach ($csvLines as $k=>$csvLine) {

                    $csvLine = $this->_getCsvValues($csvLine);
                    $splitCountries = explode(",", trim($csvLine[0]));
                    $splitRegions = explode(",", trim($csvLine[1]));

//                    $splitPostcodes = explode(",",strtoupper(trim($csvLine[3])));
                    $splitZones = explode(",",strtoupper(trim($csvLine[3])));

                    $suburbsByPostcodes = $this->_getPostcodes($splitZones);
                    $splitPostcodes = array_keys($suburbsByPostcodes);
                    $zip_to = '';

                    if ($csvLine[2] == '*' || $csvLine[2] == '') {
                        $city = '';
                    } else {
                        $city = $csvLine[2];
                    }

                    if ( $csvLine[4] == '*' || $csvLine[4] == '') {
                        $weightFrom = 0;
                    } else if (!$this->_isPositiveDecimalNumber($csvLine[4]) ) {
                        $exceptions[] = Mage::helper('shipping')->__('Invalid Weight From "%s" in the Row #%s',  $csvLine[4], ($k+1));
                    } else {
                        $weightFrom = (float)$csvLine[4];
                    }


                    if ( $csvLine[5] == '*' || $csvLine[5] == '') {
                        $weightTo = 10000000;
                    } else if (!$this->_isPositiveDecimalNumber($csvLine[5]) ) {
                        $exceptions[] = Mage::helper('shipping')->__('Invalid Weight To "%s" in the Row #%s',  $csvLine[5], ($k+1));
                    } else {
                        $weightTo = (float)$csvLine[5];
                    }

                    if ( $csvLine[6] == '*' || $csvLine[6] == '') {
                        $priceFrom = 0;
                    } else if (!$this->_isPositiveDecimalNumber($csvLine[6]) ) {
                        $exceptions[] = Mage::helper('shipping')->__('Invalid Price From "%s" in the Row #%s',  $csvLine[6], ($k+1));
                    } else {
                        $priceFrom = (float)$csvLine[6];
                    }


                    if ( $csvLine[7] == '*' || $csvLine[7] == '') {
                        $priceTo = 10000000;
                    } else if (!$this->_isPositiveDecimalNumber($csvLine[7]) ) {
                        $exceptions[] = Mage::helper('shipping')->__('Invalid Price To "%s" in the Row #%s',  $csvLine[7], ($k+1));
                    } else {
                        $priceTo = (float)$csvLine[7];
                    }

                    if ( $csvLine[8] == '*' || $csvLine[8] == '') {
                        $itemFrom = 0;
                    } else {
                        $itemFrom = $csvLine[8];
                    }


                    if ( $csvLine[9] == '*' || $csvLine[9] == '') {
                        $itemTo = 10000000;
                    } else {
                        $itemTo = $csvLine[9];
                    }

                    if ( $csvLine[11] == '*' || $csvLine[11] == '') {
                        $algorithm = '';
                    } else {
                        $algorithm=$csvLine[11];
                    }
                    if ( $csvLine[13] == '*' || $csvLine[13] == '') {
                        $sortOrder = 0;
                    } else {
                        $sortOrder=$csvLine[13];
                    }
                    foreach ($splitCountries as $country) {

                        $country = trim($country);
                        if (empty($countryCodesToIds) || !array_key_exists($country, $countryCodesToIds)) {
                            $countryId = '0';
                            if ($country != '*' && $country != '') {
                                $exceptions[] = Mage::helper('shipping')->__('Invalid Country "%s" in the Row #%s',$country, ($k+1));
                            }
                        } else {
                            $countryId = $countryCodesToIds[$country];
                        }

                        foreach ($splitRegions as $region) {

                            if (!isset($countryCodesToIds[$country])
                                || !isset($regionCodesToIds[$countryCodesToIds[$country]])
                                || !array_key_exists($region, $regionCodesToIds[$countryCodesToIds[$country]])) {
                                $regionId = '0';
                                if ($region != '*' && $region != '') {
                                    $exceptions[] = Mage::helper('shipping')->__('Invalid Region/State "%s" in the Row #%s', $region, ($k+1));
                                }
                            } else {
                                $regionId = $regionCodesToIds[$countryCodesToIds[$country]][$region];
                            }
                            foreach ($splitPostcodes as $postcode){

                                if ($postcode == '*' || $postcode == '') {
                                    $zip = '';
                                    $new_zip_to = '';
                                } else {
                                    $zip_str = explode("-", $postcode);
                                    if(count($zip_str) != 2)
                                    {
                                        $zip = trim($postcode);
                                        if (ctype_digit($postcode) && trim($zip_to) == '') {
                                            $new_zip_to = trim($postcode);
                                        } else $new_zip_to = $zip_to;
                                    }
                                    else {
                                        $zip = trim($zip_str[0]);
                                        $new_zip_to = trim($zip_str[1]);
                                    }
                                }

                                $city = $this->_getCityByPostcode($postcode, $suburbsByPostcodes);

                                if(is_array($city) && !empty($city)){
                                    foreach($city as $cityItem){
                                        $data[] = array('website_id'=>$websiteId, 'dest_country_id'=>$countryId, 'dest_region_id'=>$regionId,
                                            'dest_city'=>$cityItem, 'dest_zip'=>$zip, 'dest_zip_to'=>$new_zip_to, 'condition_name'=>$conditionName,
                                            'weight_from_value'=>$weightFrom,'weight_to_value'=>$weightTo,
                                            'price_from_value'=>$priceFrom,'price_to_value'=>$priceTo,
                                            'item_from_value'=>$itemFrom,'item_to_value'=>$itemTo,
                                            'price'=>$csvLine[10], 'algorithm'=>$algorithm, 'delivery_type'=>$csvLine[12], 'sort_order'=>$sortOrder);
                                    }
                                } else {
                                    $data[] = array('website_id'=>$websiteId, 'dest_country_id'=>$countryId, 'dest_region_id'=>$regionId,
                                        'dest_city'=>$city, 'dest_zip'=>$zip, 'dest_zip_to'=>$new_zip_to, 'condition_name'=>$conditionName,
                                        'weight_from_value'=>$weightFrom,'weight_to_value'=>$weightTo,
                                        'price_from_value'=>$priceFrom,'price_to_value'=>$priceTo,
                                        'item_from_value'=>$itemFrom,'item_to_value'=>$itemTo,
                                        'price'=>$csvLine[10], 'algorithm'=>$algorithm, 'delivery_type'=>$csvLine[12], 'sort_order'=>$sortOrder);
                                }

                                $dataDetails[] = array('country'=>$country, 'region'=>$region);
                                $counter++;
                            }
                        }

                        $dataStored = false;
                        if (!empty($exceptions)) {
                            break;
                        }
                        if($counter>1000) {
                            foreach($data as $k=>$dataLine) {
                                try {
                                    $connection->insert($table, $dataLine);
                                } catch (Exception $e) {
                                    $messageStr = Mage::helper('shipping')->__('Error# 302 - Duplicate Row #%s (Country "%s", Region/State "%s", Zip "%s")',
                                        ($k+1), $dataDetails[$k]['country'], $dataDetails[$k]['region'], $dataLine['dest_zip']);


                                    $exceptions[] = $messageStr;
                                    Mage::helper('wsalogger/log')->postWarning('premiumrate','Duplicate Row',$messageStr,$this->_debug,
                                        '302','http://wiki.webshopapps.com/troubleshooting-guide/duplicate-row-error');

                                    //$exceptions[] = Mage::helper('shipping')->__($e);
                                }
                            }
                            Mage::helper('wsacommon/shipping')->updateStatus($session,count($data));
                            $counter = 0;
                            unset($data);
                            unset($dataDetails);
                            $dataStored = true;
                        }
                    }
                }

                if(empty($exceptions) && !$dataStored) {
                    foreach($data as $k=>$dataLine) {
                        try {
                            $connection->insert($table, $dataLine);
                        } catch (Exception $e) {
                            $messageStr = Mage::helper('shipping')->__('Error# 302 - Duplicate Row #%s (Country "%s", Region/State "%s", Zip "%s")',
                                ($k+1), $dataDetails[$k]['country'], $dataDetails[$k]['region'], $dataLine['dest_zip']);
                            $exceptions[] = $messageStr;

                            Mage::helper('wsalogger/log')->postWarning('premiumrate','Duplicate Row',$messageStr,$this->_debug,
                                302,'http://wiki.webshopapps.com/troubleshooting-guide/duplicate-row-error');

                        }
                    }
                    Mage::helper('wsacommon/shipping')->updateStatus($session,count($data));

                }
                if (!empty($exceptions)) {
                    throw new Exception( "\n" . implode("\n", $exceptions) );
                }
            }
        }
    }

    private function _getCsvValues($string, $separator=",")
    {
        $elements = explode($separator, trim($string));
        for ($i = 0; $i < count($elements); $i++) {
            $nquotes = substr_count($elements[$i], '"');
            if ($nquotes %2 == 1) {
                for ($j = $i+1; $j < count($elements); $j++) {
                    if (substr_count($elements[$j], '"') > 0) {
                        // Put the quoted string's pieces back together again
                        array_splice($elements, $i, $j-$i+1, implode($separator, array_slice($elements, $i, $j-$i+1)));
                        break;
                    }
                }
            }
            if ($nquotes > 0) {
                // Remove first and last quotes, then merge pairs of quotes
                $qstr =& $elements[$i];
                $qstr = substr_replace($qstr, '', strpos($qstr, '"'), 1);
                $qstr = substr_replace($qstr, '', strrpos($qstr, '"'), 1);
                $qstr = str_replace('""', '"', $qstr);
            }
            $elements[$i] = trim($elements[$i]);
        }
        return $elements;
    }

    private function _isPositiveDecimalNumber($n)
    {
        return preg_match ("/^[0-9]+(\.[0-9]*)?$/", $n);
    }

    protected function _getPostcodes($splitZones){

        $postcodes = array();
        if(count($splitZones) == 1 && ($splitZones[0] == '*' || $splitZones[0] == '')){
            $postcodes = array('*');
        } else {

            $suburbCollection = Mage::getResourceModel('ovsuburbselector/suburb_collection')
                ->addFieldToFilter('zone', array('in' => $splitZones))
                ->addGroupBy('suburb');

            foreach ($suburbCollection as $suburb){

                if(!isset($postcodes[$suburb->getPostcode()])){
                    $postcodes[$suburb->getPostcode()] = array();
                }

                $postcodes[$suburb->getPostcode()][] = array(
                    'suburb' => $suburb->getSuburb(),
                    'zone' => $suburb->getZone()
                );
            }
        }

        return $postcodes;
    }

    protected function _getRegionByPostcode($postcode){
        $region = '';
        if($postcode != ''){
            $postcode = (string)$postcode;
            $digit = $postcode[0];
            if(strlen($postcode) == 3 || $digit === 0){
                $region = $this->_postcodesStatesMap[0];
            } else {
                $region = $this->_postcodesStatesMap[$digit];
            }
        }
        return $region;
    }

    protected function _getCityByPostcode($postcode, $suburbsByPostcodes){
        $cities = array();
        if (isset($suburbsByPostcodes[$postcode])){
            foreach($suburbsByPostcodes[$postcode] as $suburb){
                $cities[] = $suburb['suburb'];
            }
        }
        return $cities;
    }

}