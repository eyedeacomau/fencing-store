<?php

class Onlinevisions_SuburbSelector_Model_Resource_Suburb extends Mage_Core_Model_Resource_Db_Abstract
{

    /**
     * Array of countries keyed by iso2 code
     *
     * @var array
     */
    protected $_importIso2Countries;

    /**
     * Array of countries keyed by iso3 code
     *
     * @var array
     */
    protected $_importIso3Countries;

    /**
     * Associative array of countries and regions
     * [country_id][region_code] = region_id
     *
     * @var array
     */
    protected $_importRegions;


    /**
     * Errors in import process
     *
     * @var array
     */
    protected $_importErrors        = array();

    /**
     * Count of imported suburbs
     *
     * @var int
     */
    protected $_importedRows        = 0;


    protected function _construct()
    {
        $this->_init('ovsuburbselector/suburb', 'entity_id');
    }

    /**
     * Upload suburb addresses file and import data from it
     *
     * @param Varien_Object $object
     * @throws Mage_Core_Exception
     * @return Onlinevisions_SuburbSelector_Model_Resource_Suburb
     */
    public function uploadAndImport(Varien_Object $object){
        if (empty($_FILES['groups']['tmp_name']['general']['fields']['import']['value'])) {
            return $this;
        }

        $csvFile = $_FILES['groups']['tmp_name']['general']['fields']['import']['value'];

        $this->_importErrors        = array();
        $this->_importedRows        = 0;

        $io     = new Varien_Io_File();
        $info   = pathinfo($csvFile);
        $io->open(array('path' => $info['dirname']));
        $io->streamOpen($info['basename'], 'r');

        // check and skip headers
        $headers = $io->streamReadCsv();
        if ($headers === false || count($headers) < 2) {
            $io->streamClose();
            Mage::throwException(Mage::helper('ovsuburbselector')->__('Invalid CSV File. Suburb and Postcode is required fields.'));
        }

        $adapter = $this->_getWriteAdapter();
        $adapter->beginTransaction();

        try {
            $rowNumber  = 1;
            $importData = array();

//            $this->_loadDirectoryCountries();
//            $this->_loadDirectoryRegions();

            // delete old data
            $adapter->truncateTable($this->getMainTable());

            while (false !== ($csvLine = $io->streamReadCsv())) {
                $rowNumber ++;

                if (empty($csvLine)) {
                    continue;
                }

                $row = $this->_getImportRow($csvLine, $rowNumber);
                if ($row !== false) {
                    $importData[] = $row;
                }

                if (count($importData) == 5000) {
                    $this->_saveImportData($importData);
                    $importData = array();
                }
            }
            $this->_saveImportData($importData);
            $io->streamClose();
        } catch (Mage_Core_Exception $e) {
            $adapter->rollback();
            $io->streamClose();
            Mage::throwException($e->getMessage());
        } catch (Exception $e) {
            $adapter->rollback();
            $io->streamClose();
            Mage::logException($e);
            Mage::throwException(Mage::helper('ovsuburbselector')->__('An error occurred while import process.'));
        }

        $adapter->commit();

        if ($this->_importErrors) {
            $error = Mage::helper('ovsuburbselector')->__('File has not been imported. See the following list of errors: %s', implode(" \n", $this->_importErrors));
            Mage::throwException($error);
        }

        return $this;
    }

    /**
     * Validate row for import and return suburb array or false
     * Error will be add to _importErrors array
     *
     * @param array $row
     * @param int $rowNumber
     * @return array|false
     */
    protected function _getImportRow($row, $rowNumber = 0)
    {
        // validate row
        if (count($row) < 2) {
            $this->_importErrors[] = Mage::helper('ovsuburbselector')->__('Invalid fields format in the Row #%s', $rowNumber);
            return false;
        }

        // strip whitespace from the beginning and end of each row
        foreach ($row as $k => $v) {
            $row[$k] = trim($v);
        }

        // validate country
//        if (isset($this->_importIso2Countries[$row[0]])) {
//            $countryId = $this->_importIso2Countries[$row[0]];
//        } elseif (isset($this->_importIso3Countries[$row[0]])) {
//            $countryId = $this->_importIso3Countries[$row[0]];
//        } elseif ($row[0] == '*' || $row[0] == '') {
//            $countryId = '0';
//        } else {
//            $this->_importErrors[] = Mage::helper('ovsuburbselector')->__('Invalid Country "%s" in the Row #%s.', $row[0], $rowNumber);
//            return false;
//        }

        // validate region
//        if ($countryId != '0' && isset($this->_importRegions[$countryId][$row[1]])) {
//            $regionId = $this->_importRegions[$countryId][$row[1]];
//        } elseif ($row[1] == '*' || $row[1] == '') {
//            $regionId = 0;
//        } else {
//            $this->_importErrors[] = Mage::helper('ovsuburbselector')->__('Invalid Region/State "%s" in the Row #%s.', $row[1], $rowNumber);
//            return false;
//        }

        // detect suburb
        if ($row[0] == '') {
            $this->_importErrors[] = Mage::helper('ovsuburbselector')->__('Invalid suburb field in the Row #%s', $rowNumber);
            return false;
        }
        $suburb = $row[0];

        // detect postcode
        if ($row[1] == '') {
            $this->_importErrors[] = Mage::helper('ovsuburbselector')->__('Invalid postcode field in the Row #%s', $rowNumber);
            return false;
        }
        $postcode = $row[1];

        // detect zone
        if ($row[2] == '') {
            $this->_importErrors[] = Mage::helper('ovsuburbselector')->__('Invalid zone field in the Row #%s', $rowNumber);
            return false;
        }
        $zone = $row[2];

        return array(
            0, //$countryId,                 // country_id
            0, //$regionId,                  // region_id,
            $postcode,                      // postcode
            $suburb,                     // suburb
            $zone                     // zone
        );
    }

    /**
     * Save import data batch
     *
     * @param array $data
     * @return Mage_Shipping_Model_Resource_Carrier_Tablerate
     */
    protected function _saveImportData(array $data)
    {
        if (!empty($data)) {
            $columns = array('country_id', 'region_id', 'postcode', 'suburb', 'zone');
            $this->_getWriteAdapter()->insertArray($this->getMainTable(), $columns, $data);
            $this->_importedRows += count($data);
        }

        return $this;
    }


    /**
     * Load directory countries
     *
     * @return Mage_Shipping_Model_Resource_Carrier_Tablerate
     */
    protected function _loadDirectoryCountries()
    {
        if (!is_null($this->_importIso2Countries) && !is_null($this->_importIso3Countries)) {
            return $this;
        }

        $this->_importIso2Countries = array();
        $this->_importIso3Countries = array();

        /** @var $collection Mage_Directory_Model_Resource_Country_Collection */
        $collection = Mage::getResourceModel('directory/country_collection');
        foreach ($collection->getData() as $row) {
            $this->_importIso2Countries[$row['iso2_code']] = $row['country_id'];
            $this->_importIso3Countries[$row['iso3_code']] = $row['country_id'];
        }

        return $this;
    }

    /**
     * Load directory regions
     *
     * @return Mage_Shipping_Model_Resource_Carrier_Tablerate
     */
    protected function _loadDirectoryRegions()
    {
        if (!is_null($this->_importRegions)) {
            return $this;
        }

        $this->_importRegions = array();

        /** @var $collection Mage_Directory_Model_Resource_Region_Collection */
        $collection = Mage::getResourceModel('directory/region_collection');
        foreach ($collection->getData() as $row) {
            $this->_importRegions[$row['country_id']][$row['code']] = (int)$row['region_id'];
        }

        return $this;
    }

}