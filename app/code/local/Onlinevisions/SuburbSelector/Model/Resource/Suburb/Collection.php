<?php

class Onlinevisions_SuburbSelector_Model_Resource_Suburb_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{

    protected $_currentFieldName;
    protected $_formattedItems = array();

    protected function _construct()
    {
        $this->_init('ovsuburbselector/suburb');
    }

    public function addQueryFilter($query, $inputName){
        $this->_currentFieldName = Onlinevisions_SuburbSelector_Model_Suburb_Fieldsmap::getFieldByInputName($inputName);
        $this->addFieldToFilter(
            $this->_currentFieldName,
            array('like' => $query . '%')
        );

        return $this;
    }

    public function getFormattedItems(){

        if (empty($this->_formattedItems)){
            $items = $this->getItems();

            foreach($items as $item){
                $this->_formattedItems[] = array(
                    'suggestionTitle' => $item->getSuburb() . ', ' . $item->getPostcode(),
                    'suburb' => $item->getSuburb(),
                    'postcode' => $item->getPostcode()
                );
            }
        }
        return $this->_formattedItems;
    }

    public function addGroupBy($field){
        $this->getSelect()->group($field);
        return $this;
    }

}