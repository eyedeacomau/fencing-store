<?php

class Onlinevisions_SuburbSelector_Model_Suburb extends Mage_Core_Model_Abstract
{

    protected function _construct()
    {
        $this->_init('ovsuburbselector/suburb');
    }

}