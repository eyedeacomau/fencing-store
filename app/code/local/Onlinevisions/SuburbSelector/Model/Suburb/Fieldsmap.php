<?php

class Onlinevisions_SuburbSelector_Model_Suburb_Fieldsmap {

    static $_fieldsMap = array(
        'billing[city]'         => 'suburb',
        'shipping[city]'        => 'suburb',
        'billing[postcode]'     => 'postcode',
        'shipping[postcode]'    => 'postcode',
        'billing[region_id]'    => 'region_id',
        'shipping[region_id]'   => 'region_id',
        'billing[country_id]'   => 'country_id',
        'shipping[country_id]'  => 'country_id',
        //Estimate calculator
        'estimate_city'         => 'suburb',
        'estimate_postcode'     => 'postcode',
        //Customer Address
        'city'                  => 'suburb',
        'postcode'              => 'postcode'
    );

    static function getFieldByInputName($inputName){
        return self::$_fieldsMap[$inputName];
    }

}