<?php

class Onlinevisions_SuburbSelector_IndexController extends Mage_Core_Controller_Front_Action
{

    public function indexAction(){

        $query = $this->getRequest()->getParam('query', false);
        $inputName = $this->getRequest()->getParam('inputname', 'billing[city]');
        $items = array();
        if ($query){
            $items = Mage::getResourceModel('ovsuburbselector/suburb_collection')
                ->addQueryFilter($query, $inputName)
                ->getFormattedItems();
        }

        $result = array(
            "query" => $query,
            "inputName"      => $inputName,
            "fieldName"      => Onlinevisions_SuburbSelector_Model_Suburb_Fieldsmap::getFieldByInputName($inputName),
            "items" => $items
        );

        $this->getResponse()->setBody(Zend_Json::encode($result));

    }

}