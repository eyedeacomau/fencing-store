<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/**
 * Create table 'ovsuburbselector/suburb'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('ovsuburbselector/suburb'))
    ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Primary key')
    ->addColumn('country_id', Varien_Db_Ddl_Table::TYPE_TEXT, 4, array(
        'nullable'  => false,
        'default'   => '0',
    ), 'Coutry ISO/2 or ISO/3 code')
    ->addColumn('region_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable'  => false,
        'default'   => '0',
    ), 'Region Id')
    ->addColumn('postcode', Varien_Db_Ddl_Table::TYPE_TEXT, 10, array(
        'nullable'  => false,
    ), 'Post Code (Zip)')
    ->addColumn('suburb', Varien_Db_Ddl_Table::TYPE_TEXT, 100, array(
        'nullable'  => false,
    ), 'Suburb (City)')

    ->addIndex($installer->getIdxName('ovsuburbselector/suburb', 'suburb', Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX),
        'suburb',
        array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX)
    )
    ->addIndex($installer->getIdxName('ovsuburbselector/suburb', 'postcode', Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX),
        'postcode',
        array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX)
    )
    ->setComment('Suburb Addresses');
$installer->getConnection()->createTable($table);

$installer->endSetup();