<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->getConnection()
    ->addColumn(
        $installer->getTable('ovsuburbselector/suburb'),
        'zone',
        array(
            'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
            'length'    => 255,
            'nullable'  => true,
            'default'   => null,
            'comment'   => 'Shipping Zones'
        )
    );

$installer->endSetup();