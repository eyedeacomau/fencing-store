<?php
/**
 *
 */
require_once( Mage::getModuleDir('controllers','Magentomasters_Supplier') . DS . 'IndexController.php' );
/**
 *
 * Enter description here ...
 * @author psocha
 *
 */
class Polcode_SupplierExtended_IndexController extends Magentomasters_Supplier_IndexController
{
    /**
     * Predispatch:
     *
     * @return Mage_Core_Controller_Front_Action
     */
    public function preDispatch()
    {

        //
        parent::preDispatch();

        $username = $this->getRequest()->getParam('user');
        $token = $this->getRequest()->getParam('token');
        $orderId = (int)$this->getRequest()->getParam('order');

        if ( strlen($username) && strlen($token) ) {

            // Check logining user info
            $id = Mage::getModel('supplier/redirect')->checkSupplierInfoToken( $username, $token );


            $homePath = 'supplier/order';
            //
            if($orderId){
                if ($this->getRequest()->getActionName() == 'complete') {
                    $homePath = 'supplier/shipping/ship/order_id/' . $orderId;
                } else {
                    $homePath = 'supplier/order/view/order_id/' . $orderId;
                }
            }

            //
            if( $id ) {
                Mage::getSingleton('core/session')->setData( 'supplierId' , $id['id'] );
                $this->_redirect( $homePath );
            } else {
                Mage::getSingleton('core/session')->addError($this->__('Wrong login or password'));
                $this->_redirect( 'supplier/index/index/error/true' );
            }
        }

        //
        return $this;
    }

    /**
     *
     * Enter description here ...
     */
    public function confirmAction()
    {
    }

    /**
     *
     * Enter description here ...
     */
    public function completeAction()
    {
    }
}