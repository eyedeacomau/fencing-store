<?php
/**
 *
 */
require_once( Mage::getModuleDir('controllers','Magentomasters_Supplier') . DS . 'OrderController.php' );
/**
 *
 * Enter description here ...
 * @author psocha
 *
 */
class Polcode_SupplierExtended_OrderController extends Magentomasters_Supplier_OrderController {

    /**
     *
     * Enter description here ...
     */
    public function viewAction() {
        $session = Mage::getSingleton('core/session');
        $supplierId = $session->getData('supplierId');
        $orderId = $this->getRequest()->getParam('order_id');
        $order = Mage::getModel('sales/order')->load($orderId);
        Mage::register('sales_order', $order);
        Mage::register('order', $order);
        if( $supplierId && $supplierId != "logout" && $orderId) {
        	$check = Mage::getModel('supplier/order')->checkOrderAuth($supplierId,$orderId);
            if(!$check){
            	$this->_redirectUrl(Mage::getUrl() . "supplier/order");
			} else {
            	$this->loadLayout()->renderLayout();

            	//Sohi_Debug::dump($order->getStatus(), '$order->getStatus()');
            	if ($order->getStatus() == 'sent_to_warehouse') {
            	    $order->setStatus('warehouse_processing');
            	    $order->save();
					//Online Visions
					//send email to customer
					$templateId = 7;
					$this->_sendStatusMail($order,$templateId);
            	}
	
			}
		} else {
            $redirectPath = Mage::getUrl() . "supplier/";
            $this->_redirectUrl( $redirectPath );
        }
        if( $this->getRequest()->getParam( 'error' ) ){
            Mage::getSingleton('core/session')->addError($this->__('The username or password you entered is incorrect'));
        }
    }
	
	private  function _sendStatusMail($order,$templateId)
    {

		$emailTemplate = Mage::getModel('core/email_template')->load($templateId);	 
   		$salesData['email'] = Mage::getStoreConfig('trans_email/ident_sales/email');
        $salesData['name'] = Mage::getStoreConfig('trans_email/ident_sales/name');
        $emailTemplate->setTemplateSubject(Mage::app()->getStore()->getFrontendName().': Order # '.$order->getIncrementId().' update');
        $emailTemplate->setSenderName($salesData['name']);
        $emailTemplate->setSenderEmail($salesData['email']);
 		
        $emailTemplateVariables['order']  = $order;
        $emailTemplate->send($order->getCustomerEmail(), $order->getCustomerFirstname() . ' ' . $order->getCustomerLastname(), $emailTemplateVariables);
    }
}