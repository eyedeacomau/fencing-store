<?php

require_once 'Magentomasters/Supplier/controllers/ShippingController.php';


class Polcode_SupplierExtended_ShippingController extends Magentomasters_Supplier_ShippingController
{

    protected $_partShipped = false;

    public function addshipmentAction(){

        $session = Mage::getSingleton('core/session');
        $supplierId = $session->getData('supplierId');
        $orderId = $this->getRequest()->getParam('orderid');

        if( $supplierId && $supplierId != "logout") {

            $post = $this->getRequest()->getPost();

            if ($post) {

                $orderId = $post['order_id'];
                $itemsQty = $post['ship_qty'];
                $tracking = $post['tracking'];
                $_error = false;

                try {
                    $this->completeAndShip($orderId,$itemsQty,$tracking);
                } catch (Exception $e) {
                    Mage::getSingleton('core/session')->addError($e->getMessage());
//                    $this->_redirectUrl( Mage::getUrl() . 'supplier/order');
                    $this->_redirectReferer();
                    $_error = true;
                }

                if ($this->_partShipped === true){
                    Mage::getSingleton('core/session')->addSuccess("Successfully Part Shipped");
                    $this->_redirectReferer();
                } else if (!$_error) {
                    Mage::getSingleton('core/session')->addSuccess("Successfully Shipped");
                    $this->_redirectUrl(Mage::getUrl().'supplier/order');
                }

            }



        } else {
            $redirectPath = Mage::getUrl() . "supplier/";
            $this->_redirectUrl( $redirectPath );
        }
    }

    public function completeAndShip($orderId,$itemsQty,$tracking){
        $email = true; // <-- Must be users email address  $order->getCustomerEmail()
        $carrier = 'custom';
        $includeComment = false;
        $comment = "The order is shipped by the supplier";
        $order = Mage::getModel('sales/order')->load($orderId);
        $convertor = Mage::getModel('sales/convert_order');
        $shipment = $convertor->toShipment($order);

        foreach ($order->getAllItems() as $k=>$orderItem) {

            if (!$orderItem->getQtyToShip()) {
                continue;
            }
            if ($orderItem->getIsVirtual()) {
                continue;
            }

            $item = $convertor->itemToShipmentItem($orderItem);

            $productId = $orderItem->getProductId();

            if($itemsQty[$productId]) {
                $item->setQty($itemsQty[$productId]);
                $shipment->addItem($item);
            }
        }

        $carrierTitle = NULL;

        if ($carrier == 'custom') {
            $carrierTitle = 'Playtimes';
        }
        foreach ($tracking as $_key => $data) {
            if ($_key == '__index__') continue;
            if (isset($data['carrier'])){
                $carrier = $data['carrier'] ? $data['carrier'] : $carrier;
            }

            $data['title'] = $carrier;
            $data['carrier_code'] = $carrier;
            $track = Mage::getModel('sales/order_shipment_track')->addData($data);
            $shipment->addTrack($track);
        }

        $shipment->register();
        $shipment->addComment($comment, $email && $includeComment);
        $shipment->setEmailSent(true);
        $shipment->getOrder()->setIsInProcess(true);

        $transactionSave = Mage::getModel('core/resource_transaction')
            ->addObject($shipment)
            ->addObject($shipment->getOrder())
            ->save();

        $shipment->sendEmail($email, ($includeComment ? $comment : ''));

        if ($shipment->getOrder()->canShip() && Mage::getBlockSingleton('supplier/header')->canShip($shipment->getOrder()->getId())) {
//            $_block = Mage::getBlockSingleton('supplier/header');

            $shipment->getOrder()->setStatus('part_shipped');
            $shipment->getOrder()->addStatusToHistory($order->getStatus(), 'Order is Part Shipped', false);
            $this->_partShipped = true;

        } else {
            $order->setStatus('Complete');
            $order->addStatusToHistory($order->getStatus(), 'Order Completed because every item have been shipped', false);
            $this->_partShipped = false;
        }
        $transactionSave = Mage::getModel('core/resource_transaction')
            ->addObject($shipment)
            ->addObject($shipment->getOrder())
            ->save();


    }

}