<?php

/**
 * Product:       Xtento_AdvancedOrderStatus (1.0.9)
 * ID:            XUBaCRegHwrjzSZ9zCgHDeRMILMQzYOH6cuJR2LVNOc=
 * Packaged:      2014-01-06T07:00:01+00:00
 * Last Modified: 2013-03-14T15:18:27+01:00
 * File:          app/code/local/Xtento/AdvancedOrderStatus/Model/Sales/Order/Status/History.php
 * Copyright:     Copyright (c) 2013 XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

class Xtento_AdvancedOrderStatus_Model_Sales_Order_Status_History extends Mage_Sales_Model_Order_Status_History
{
    public function setIsCustomerNotified($flag = null)
    {
        if (Mage::registry('advancedorderstatus_notifications') !== NULL && Mage::registry('advancedorderstatus_notified')) {
            $flag = 1;
        }
        parent::setIsCustomerNotified($flag);
    }
}