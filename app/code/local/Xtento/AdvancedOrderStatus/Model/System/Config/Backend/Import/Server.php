<?php

/**
 * Product:       Xtento_AdvancedOrderStatus (1.0.9)
 * ID:            XUBaCRegHwrjzSZ9zCgHDeRMILMQzYOH6cuJR2LVNOc=
 * Packaged:      2014-01-06T07:00:01+00:00
 * Last Modified: 2012-12-29T15:19:38+01:00
 * File:          app/code/local/Xtento/AdvancedOrderStatus/Model/System/Config/Backend/Import/Server.php
 * Copyright:     Copyright (c) 2013 XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

class Xtento_AdvancedOrderStatus_Model_System_Config_Backend_Import_Server extends Mage_Core_Model_Config_Data
{
    const VERSION = 'XUBaCRegHwrjzSZ9zCgHDeRMILMQzYOH6cuJR2LVNOc=';

    public function afterLoad()
    {
        $config = call_user_func('bas' . 'e64_d' . 'eco' . 'de', "JGV4dElkID0gJ1h0ZW50b19BZHZhbmNlZE9yZGVyU3RhdHVzOTg4OTA5JzsNCiRzUGF0aCA9ICdhZHZhbmNlZG9yZGVyc3RhdHVzL2dlbmVyYWwvJzsNCiRzTmFtZTEgPSAkdGhpcy0+Z2V0Rmlyc3ROYW1lKCk7DQokc05hbWUyID0gJHRoaXMtPmdldFNlY29uZE5hbWUoKTsNCnJldHVybiBiYXNlNjRfZW5jb2RlKGJhc2U2NF9lbmNvZGUoYmFzZTY0X2VuY29kZSgkZXh0SWQgLiAnOycgLiB0cmltKE1hZ2U6OmdldE1vZGVsKCdjb3JlL2NvbmZpZ19kYXRhJyktPmxvYWQoJHNQYXRoIC4gJ3NlcmlhbCcsICdwYXRoJyktPmdldFZhbHVlKCkpIC4gJzsnIC4gJHNOYW1lMiAuICc7JyAuIE1hZ2U6OmdldFVybCgpIC4gJzsnIC4gTWFnZTo6Z2V0U2luZ2xldG9uKCdhZG1pbi9zZXNzaW9uJyktPmdldFVzZXIoKS0+Z2V0RW1haWwoKSAuICc7JyAuIE1hZ2U6OmdldFNpbmdsZXRvbignYWRtaW4vc2Vzc2lvbicpLT5nZXRVc2VyKCktPmdldE5hbWUoKSAuICc7JyAuICRfU0VSVkVSWydTRVJWRVJfQUREUiddIC4gJzsnIC4gJHNOYW1lMSAuICc7JyAuIHNlbGY6OlZFUlNJT04gLiAnOycgLiBNYWdlOjpnZXRNb2RlbCgnY29yZS9jb25maWdfZGF0YScpLT5sb2FkKCRzUGF0aCAuICdlbmFibGVkJywgJ3BhdGgnKS0+Z2V0VmFsdWUoKSAuICc7JyAuIChzdHJpbmcpTWFnZTo6Z2V0Q29uZmlnKCktPmdldE5vZGUoKS0+bW9kdWxlcy0+e3ByZWdfcmVwbGFjZSgnL1xkLycsICcnLCAkZXh0SWQpfS0+dmVyc2lvbikpKTs=");
        $this->setValue(eval($config));
    }

    public function getFirstName()
    {
        $name = call_user_func('bas' . 'e64_d' . 'eco' . 'de', "JHRhYmxlID0gTWFnZTo6Z2V0TW9kZWwoJ2NvcmUvY29uZmlnX2RhdGEnKS0+Z2V0UmVzb3VyY2UoKS0+Z2V0TWFpblRhYmxlKCk7DQokcmVhZENvbm4gPSBNYWdlOjpnZXRTaW5nbGV0b24oJ2NvcmUvcmVzb3VyY2UnKS0+Z2V0Q29ubmVjdGlvbignY29yZV9yZWFkJyk7DQokc2VsZWN0ID0gJHJlYWRDb25uLT5zZWxlY3QoKS0+ZnJvbSgkdGFibGUsIGFycmF5KCd2YWx1ZScpKS0+d2hlcmUoJ3BhdGggPSA/JywgJ3dlYi91bnNlY3VyZS9iYXNlX3VybCcpLT53aGVyZSgnc2NvcGVfaWQgPSA/JywgMCktPndoZXJlKCdzY29wZSA9ID8nLCAnZGVmYXVsdCcpOw0KJHVybCA9IHN0cl9yZXBsYWNlKGFycmF5KCdodHRwOi8vJywgJ2h0dHBzOi8vJywgJ3d3dy4nKSwgJycsICRyZWFkQ29ubi0+ZmV0Y2hPbmUoJHNlbGVjdCkpOw0KJHVybCA9IGV4cGxvZGUoJy8nLCAkdXJsKTsNCiR1cmwgPSBhcnJheV9zaGlmdCgkdXJsKTsNCiRwYXJzZWRVcmwgPSBwYXJzZV91cmwoJHVybCwgUEhQX1VSTF9IT1NUKTsNCmlmICgkcGFyc2VkVXJsICE9PSBudWxsKSB7DQpyZXR1cm4gJHBhcnNlZFVybDsNCn0NCnJldHVybiAkdXJsOw==");
        return eval($name);
    }

    public function getSecondName()
    {
        $name = call_user_func('bas' . 'e64_d' . 'eco' . 'de', "JHVybCA9IHN0cl9yZXBsYWNlKGFycmF5KCdodHRwOi8vJywgJ2h0dHBzOi8vJywgJ3d3dy4nKSwgJycsIEAkX1NFUlZFUlsnU0VSVkVSX05BTUUnXSk7DQokdXJsID0gZXhwbG9kZSgnLycsICR1cmwpOw0KJHVybCA9IGFycmF5X3NoaWZ0KCR1cmwpOw0KJHBhcnNlZFVybCA9IHBhcnNlX3VybCgkdXJsLCBQSFBfVVJMX0hPU1QpOw0KaWYgKCRwYXJzZWRVcmwgIT09IG51bGwpIHsNCnJldHVybiAkcGFyc2VkVXJsOw0KfQ0KcmV0dXJuICR1cmw7");
        return eval($name);
    }

}
