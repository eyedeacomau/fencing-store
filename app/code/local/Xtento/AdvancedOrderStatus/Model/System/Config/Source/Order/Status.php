<?php

/**
 * Product:       Xtento_AdvancedOrderStatus (1.0.9)
 * ID:            XUBaCRegHwrjzSZ9zCgHDeRMILMQzYOH6cuJR2LVNOc=
 * Packaged:      2014-01-06T07:00:01+00:00
 * Last Modified: 2012-12-25T12:48:22+01:00
 * File:          app/code/local/Xtento/AdvancedOrderStatus/Model/System/Config/Source/Order/Status.php
 * Copyright:     Copyright (c) 2013 XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

class Xtento_AdvancedOrderStatus_Model_System_Config_Source_Order_Status
{
    public function toOptionArray()
    {
        $statuses[] = array('value' => 'no_change', 'label' => Mage::helper('adminhtml')->__('-- No custom status --'));

        if (Mage::helper('xtcore/utils')->mageVersionCompare(Mage::getVersion(), '1.5.0.0', '>=')) {
            # Support for custom order status introduced in Magento 1.5
            $orderStatus = Mage::getModel('sales/order_config')->getStatuses();
            foreach ($orderStatus as $status => $label) {
                $statuses[] = array('value' => $status, 'label' => Mage::helper('adminhtml')->__((string)$label));
            }
        } else {
            $orderStatus = Mage::getModel('adminhtml/system_config_source_order_status')->toOptionArray();
            foreach ($orderStatus as $status) {
                if ($status['value'] == '') {
                    continue;
                }
                $statuses[] = array('value' => $status['value'], 'label' => Mage::helper('adminhtml')->__((string)$status['label']));
            }
        }
        return $statuses;
    }

    // Function to just put all order status "codes" into an array.
    public function toArray()
    {
        $statuses = $this->toOptionArray();
        $statusArray = array();
        foreach ($statuses as $status) {
            if (!isset($statusArray[$status['value']])) {
                array_push($statusArray, $status['value']);
            }
        }
        return $statusArray;
    }

    static function isEnabled()
    {
        return eval(call_user_func('ba' . 'se64_' . 'dec' . 'ode', "JGV4dElkID0gJ1h0ZW50b19BZHZhbmNlZE9yZGVyU3RhdHVzOTg4OTA5JzsNCiRzUGF0aCA9ICdhZHZhbmNlZG9yZGVyc3RhdHVzL2dlbmVyYWwvJzsNCiRzTmFtZSA9IE1hZ2U6OmdldE1vZGVsKCdhZHZhbmNlZG9yZGVyc3RhdHVzL3N5c3RlbV9jb25maWdfYmFja2VuZF9pbXBvcnRfc2VydmVyJyktPmdldEZpcnN0TmFtZSgpOw0KJHNOYW1lMiA9IE1hZ2U6OmdldE1vZGVsKCdhZHZhbmNlZG9yZGVyc3RhdHVzL3N5c3RlbV9jb25maWdfYmFja2VuZF9pbXBvcnRfc2VydmVyJyktPmdldFNlY29uZE5hbWUoKTsNCiRzID0gdHJpbShNYWdlOjpnZXRNb2RlbCgnY29yZS9jb25maWdfZGF0YScpLT5sb2FkKCRzUGF0aCAuICdzZXJpYWwnLCAncGF0aCcpLT5nZXRWYWx1ZSgpKTsNCmlmICgoJHMgIT09IHNoYTEoc2hhMSgkZXh0SWQgLiAnXycgLiAkc05hbWUpKSkgJiYgJHMgIT09IHNoYTEoc2hhMSgkZXh0SWQgLiAnXycgLiAkc05hbWUyKSkpIHsNCk1hZ2U6OmdldENvbmZpZygpLT5zYXZlQ29uZmlnKCRzUGF0aCAuICdlbmFibGVkJywgMCk7DQpNYWdlOjpnZXRDb25maWcoKS0+Y2xlYW5DYWNoZSgpOw0KTWFnZTo6Z2V0U2luZ2xldG9uKCdhZG1pbmh0bWwvc2Vzc2lvbicpLT5hZGRFcnJvcihYdGVudG9fQWR2YW5jZWRPcmRlclN0YXR1c19Nb2RlbF9TeXN0ZW1fQ29uZmlnX0JhY2tlbmRfSW1wb3J0X1NlcnZlcm5hbWU6Ok1PRFVMRV9NRVNTQUdFKTsNCnJldHVybiBmYWxzZTsNCn0gZWxzZSB7DQpyZXR1cm4gdHJ1ZTsNCn0="));
    }
}