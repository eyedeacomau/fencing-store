/**
 * MagePlace Gallery Extension
 *
 * @copyright   Copyright (c) 2014 Mageplace. (http://www.mageplace.com)
 * @license     http://www.mageplace.com/disclaimer.html
 */

var MP = {
	massactionPositionInput: '.massaction-position-input',
	albumId: 0,
	albumIdFilterName: '',
	positionFieldName: ''
};

varienGridMassaction.addMethods({
	mpPhotoGridApply: function (savePosName) {
		var item = this.getSelectedItem();
		if (!item) {
			this.validator.validate();
			return;
		}

		if (item.id != savePosName) {
			return this.apply();
		}

		if(!MP.albumId && MP.albumIdFilterName && MP.positionFieldName) {
			if($$('input[name^="' + MP.positionFieldName + '"]').length > 0) {
				var albumId = $$('select[name="' + MP.albumIdFilterName + '"]').first().value;
				if(albumId ) {
					MP.albumId = parseInt(albumId);
				}
			}
		}

		if (MP.albumId <= 0) {
			alert('Please apply album filter first');
			return;
		} else if (item.url) {
			item.url += (item.url.slice(-1) == '/' ? '' : '/') + 'album_id/' + MP.albumId;
		}

		if (this.useAjax && item.url) {
			new Ajax.Request(item.url, {
				'method': 'post',
				'parameters': this.form.serialize(true) + Form.serializeElements($$(MP.massactionPositionInput)),
				'onComplete': this.onMassactionComplete.bind(this)
			});
		} else if (item.url) {
			this.form.action = item.url;
			$$(MP.massactionPositionInput).each(function (el) {
				el.type = 'hidden';
				this.form.insert({bottom: el});
			}, this);
			this.form.submit();
		}
	}
});

var mpGridAction = {
	execute: function (select) {
		if (!select.value || !select.value.isJSON()) {
			return;
		}

		var config = select.value.evalJSON();
		if (config.confirm && !window.confirm(config.confirm)) {
			select.options[0].selected = true;
			return;
		}

		if (config.position) {
			if(!MP.albumId && MP.albumIdFilterName && MP.positionFieldName) {
				if($$('input[name^="' + MP.positionFieldName + '"]').length > 0) {
					var albumId = $$('select[name="' + MP.albumIdFilterName + '"]').first().value;
					if(albumId ) {
						MP.albumId = parseInt(albumId);
					}
				}
			}

			if (MP.albumId <= 0) {
				alert('Please apply album filter first');
				return;
			} else {
				config.href += (config.href.slice(-1) == '/' ? '' : '/') + 'album_id/' + MP.albumId;
			}

			var posValue = 0;
			var input = select.up().up().select(MP.massactionPositionInput).first();
			if (Object.isElement(input)) {
				posValue = input.value;
			}
			config.href += (config.href.slice(-1) == '/' ? '' : '/') + 'position/' + posValue;
		}


		if (config.popup) {
			var win = window.open(config.href, 'action_window', 'width=500,height=600,resizable=1,scrollbars=1');
			win.focus();
			select.options[0].selected = true;
		} else {
			setLocation(config.href);
		}
	}
};


FormElementDependenceController.addMethods({
	initialize: function (elementsMap, config) {
		if (config) {
			this._config = config;
		}

		for (var idTo in elementsMap) {
			for (var idFrom in elementsMap[idTo]) {
				if ($(idFrom)) {
					Event.observe($(idFrom), 'change', this.trackChange.bindAsEventListener(this, idTo, elementsMap[idTo]));
					this.trackChange(null, idTo, elementsMap[idTo]);
				} else {
					this.trackChange(null, idTo, elementsMap[idTo]);
				}
			}
		}
	},

	trackChange: function (e, idTo, valuesFrom) {
		var isSystemAction = location.href.search('system_config/edit/section/mpgallery') >= 0;

		var shouldShowUp = true;
		for (var idFrom in valuesFrom) {
			var from = $(idFrom);
			if (valuesFrom[idFrom] instanceof Array) {
				if (!from || valuesFrom[idFrom].indexOf(from.value) == -1) {
					shouldShowUp = false;
				}
			} else {
				if (!from || from.value != valuesFrom[idFrom]) {
					shouldShowUp = false;
				}
			}
		}

		if (shouldShowUp) {
			var currentConfig = this._config;
			$(idTo).up(this._config.levels_up).select('input', 'select', 'td').each(function (item) {
				// don't touch hidden inputs (and Use Default inputs too), bc they may have custom logic
				if ((!item.type || item.type != 'hidden')
					&& !($('use_config_' + item.id) && $('use_config_' + item.id).checked)
					&& !($(item.id + '_inherit') && $(item.id + '_inherit').checked)
					&& !(currentConfig.can_edit_price != undefined && !currentConfig.can_edit_price)) {
					item.disabled = false;
				}
			});
			$(idTo).up(this._config.levels_up).show();
		} else {
			$(idTo).up(this._config.levels_up).select('input', 'select', 'td').each(function (item) {
				// don't touch hidden inputs (and Use Default inputs too), bc they may have custom logic
				if ((!item.type || item.type != 'hidden')
					&& !($('use_config_' + item.id) && $('use_config_' + item.id).checked)
					&& !($(item.id + '_inherit') && $(item.id + '_inherit').checked)) {
					if (isSystemAction && item.tagName.toUpperCase() == 'SELECT') {
						item.value = 0;
					} else {
						item.disabled = true;
					}
				}
			});
			$(idTo).up(this._config.levels_up).hide();
		}
	}
});

MP.review = {
	photoUrl: null,
	photoInfoUrl: null,
	formHidden: true,

	gridRowClick: function (data, click) {
		if (Event.findElement(click, 'TR').title) {
			MP.review.photoInfoUrl = Event.findElement(click, 'TR').title;
			MP.review.loadPhotoData();
			MP.review.showForm();
			MP.review.formHidden = false;
		}
	},

	loadPhotoData: function () {
		var con = new Ext.lib.Ajax.request('POST', MP.review.photoInfoUrl, {success: MP.review.reqSuccess, failure: MP.review.reqFailure}, {form_key: FORM_KEY});
	},

	showForm: function () {
		toggleVis('saveandcontinue_button');
		toggleVis('reset_button');
		toggleVis('save_button');
		toggleVis('photo_grid');
		toggleVis('edit_form');
	},

	reqSuccess: function (transport) {
		var response = Ext.util.JSON.decode(transport.responseText);
		if (response.error) {
			alert(response.message);
		} else if (response.id) {
			$("photo_id").value = response.id;

			$("photo_info").innerHTML = '<a href="' + response.edit_url + '" target="_blank"><img src="' + response.image_src + '"/></a>'
				+ '<br />'
				+ '<a href="' + response.edit_url + '" target="_blank">' + response.name + '</a>';
		} else if (response.message) {
			alert(response.message);
		}
	},

	reqFailure: function(transport) {
		console.log(transport);
	}
};

MP.System = {};
MP.System.Config = {};
MP.System.Config.PhotoUpload = Class.create();
MP.System.Config.PhotoUpload.prototype = {
	elEnableId: 'mpgallery_photo_upload_enabled',

	initialize: function () {
	},

	process: function () {
		if (!Object.isElement($(this.elEnableId))) {
			return;
		}

		$(this.elEnableId).observe('change', this.toggle.bind(this));

		this.toggle();
	},

	toggle: function () {
		if (!this.isEnable()) {
			$('mpgallery_photo_upload_enabled').up().up().up().select('select').each(function (el){el.value = 0})
		}
	},

	isEnable: function () {
		return $(this.elEnableId).value == 1;
	}
}

Event.observe(window, 'load', function () {
	new MP.System.Config.PhotoUpload().process();
});