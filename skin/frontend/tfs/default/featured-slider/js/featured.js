(function($) {
	jQuery.fn.featuredSlider=function(args){
		var defaults={
				width		:1000,
				origLw		:500,
				origLh		:400,
				origSw		:250,
				origSh		:200,
		}
		options=jQuery.extend({},defaults,args);

		return this.each(function(idx,elm){
			var excerpt=jQuery(this).find(".featured_excerpt"),
			sub_excerpt=jQuery(this).find(".featured_slide_sub_excerpt"),
			id=jQuery(this).attr('id'),
			eshortcode=jQuery(this).find('.featured_slider_eshortcode'),
			self=this,
			sldrH=jQuery(this).height();
			var ht=excerpt.height();
			excerpt.height(0);
			
			jQuery(this).find(".featured_slide_left").hover(
			function(){excerpt.stop(true,true).animate({height: ht+"px"}, 400, "easeInSine");},
			function(){excerpt.animate({height: "0px"}, 300, "easeOutSine");}
			);
			var ht_sub=sub_excerpt.height();
			sub_excerpt.height(0);
			jQuery(this).find(".featured_slide_sub_right").hover(function(){jQuery(this).find(".featured_slide_sub_excerpt").stop(true,true).animate({height: ht_sub+"px"}, 400, "easeInSine");},
				function(){jQuery(this).find(".featured_slide_sub_excerpt").animate({height: "0px"}, 300, "easeOutSine");});
			this.featuredSliderSize=function(){
				var largeSlide=jQuery(this).find('.featured_slide_left'),
					smallSlideContainer=jQuery(this).find('.featured_slide_right'),
					smallSlide=jQuery(this).find('.featured_slide_sub_right'),
					wrapWidth=jQuery(this).width(),
					ht=0,
					lsW=largeSlide.outerWidth(true);
				if(wrapWidth<=options.origLw){
					largeSlide.css({'float':'none','width':wrapWidth+'px'});
					smallSlideContainer.css({'float':'none','width':wrapWidth+'px'});
					smallSlide.css({'width':(wrapWidth/2)+'px'});
					ht=(options.origLh*wrapWidth)/options.origLw; 
					jQuery(this).height(ht*2);
					largeSlide.height(ht);
					smallSlideContainer.height(ht);
					smallSlide.height(ht/2);
				}
				else{
					var lswidth=(options.origLw * 100 / options.width),
					sswidth=(options.origSw * 100 / options.width);
					largeSlide.css({'float':'','width':(lswidth*wrapWidth/100)+'px'});
					smallSlideContainer.css({'float':'','width':(sswidth*wrapWidth/100)+'px'});
					smallSlide.css({'width':(sswidth*wrapWidth)/(2*100)+'px'});
					ht=(options.origLh*wrapWidth)/options.width;
					jQuery(this).height(ht);
					largeSlide.height(ht);
					smallSlideContainer.height(ht);
					smallSlide.height(ht/2);
				}
				//video css
				if(eshortcode.length > 0)
				{
					if(eshortcode.parents(".featured_slide_left").length > 0)
					{ 	jQuery(this).find(".featured_slide_left .featured_slider_eshortcode").height(ht);
						jQuery(this).find(".featured_slide_left .featured_slider_eshortcode").css("width","100%");
					}
					if(eshortcode.parents(".featured_slide_right").length > 0)
					{
						jQuery(this).find(".featured_slide_right .featured_slider_eshortcode").height(ht/2);
						jQuery(this).find(".featured_slide_right .featured_slider_eshortcode").css("width","100%");
					}
				}
				//Make fonts of side slides smaller
				var featuredRw=options.origSw;
				var rItemW=jQuery(this).find('.featured_slide_sub_right').width();
				var fRw=(featuredRw*35)/100;
				if(rItemW <= fRw)
				{
					jQuery(this).find(".featured_slideri").each(function(idx,el){
						var sideh2=jQuery(el).find(".featured_slide_sub_right .featured_slide_sub_content h2");
						sideh2.addClass('featuredSmallF');
					});
				}
				else{
					jQuery(this).find(".featured_slideri").each(function(idx,el){
						var sideh2=jQuery(el).find(".featured_slide_sub_right .featured_slide_sub_content h2");
						sideh2.removeClass('featuredSmallF');
					});
				}
				return this;
			};
			this.featuredSliderSize();
			//On Window Resize
			jQuery(window).resize(function() { 
				self.featuredSliderSize();
			});
		});		
	}
})(jQuery);
